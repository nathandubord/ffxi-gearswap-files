function get_sets()
    send_command('bind numpad1 gs c TP')
    send_command('bind numpad2 gs c NM')

    sets.TP = {}
    sets.NukingMode = {}
    sets.Enfeebling = {}

    TPMode = 'Standard'
    NukingMode = 'Standard'

    -- Typical all purpose idle set
    sets.Idle = {
        main="Daybreak",
        sub="Beatific shield +1",
        ammo="Homiliary",
        head={ name="Viti. Chapeau +3", augments={'Enfeebling Magic duration','Magic Accuracy',}},
        body="Lethargy sayon +3",
        hands="Nyame Gauntlets",
        legs="Carmine Cuisses +1",
        feet="Nyame Sollerets",
        neck="Warder's Charm +1",
        waist="Carrier's sash",
        left_ear="Genmei Earring",
        right_ear="Hearty Earring",
        left_ring="Defending Ring",
        right_ring="Stikini Ring +1",
        back="Moonlight cape",
    }

    sets.WS = {}
    sets.WS['Chant du Cygne'] = {
        ammo="Yetshila +1",
        head={ name="Blistering Sallet +1", augments={'Path: A',}},
        body="Malignance Tabard",
        hands="Malignance Gloves",
        legs={ name="Nyame Flanchard", augments={'Path: B',}},
        feet="Aya. Gambieras +2",
        neck="Fotia Gorget",
        waist="Fotia Belt",
        left_ear="Sherida Earring",
        right_ear="Brutal Earring",
        left_ring="Begrudging Ring",
        right_ring="Ilabrat Ring",
        back={ name="Sucellos's Cape", augments={'DEX+20','Accuracy+20 Attack+20','"Dual Wield"+10',}},
    }

    sets.CDC = sets.WS['Chant du Cygne']

    sets.WS['Aeolian Edge'] = {
        ammo="Sroda Tathlum",
        head="Leth. Chappel +3",
        body={ name="Nyame Mail", augments={'Path: B',}},
        hands="Jhakri Cuffs +2",
        legs="Leth. Fuseau +3",
        feet="Leth. Houseaux +3",
        neck="Sibyl scarf",
        waist="Orpheus's Sash",
        left_ear={ name="Moonshade Earring", augments={'Accuracy+4','TP Bonus +250',}},
        right_ear="Malignance Earring",
        left_ring="Freke ring",
        right_ring="Cornelia's Ring",
        back={ name="Sucellos's Cape", augments={'MND+20','Mag. Acc+20 /Mag. Dmg.+20','MND+10','Weapon skill damage +10%',}},
    }

    sets.WS['Savage Blade'] = {
        ammo="Oshasha's Treatise",
        head="Nyame Helm",
        body={ name="Nyame Mail", augments={'Path: B',}},
        hands="Nyame Gauntlets",
        legs={ name="Nyame Flanchard", augments={'Path: B',}},
        feet="Leth. Houseaux +3",
        neck="Rep. Plat. Medal",
        waist="Sailfi Belt +1",
        left_ear={ name="Moonshade Earring", augments={'Accuracy+4','TP Bonus +250',}},
        right_ear="Ishvara earring",
        left_ring="Sroda Ring",
        right_ring="Cornelia's Ring",
        back={ name="Sucellos's Cape", augments={'STR+20','Accuracy+20 Attack+20','STR+10','Weapon skill damage +10%',}},
    }

    sets.WS['Seraph Blade'] = {
        ammo="Sroda Tathlum",
        head="Leth. Chappel +3",
        body={ name="Nyame Mail", augments={'Path: B',}},
        hands="Leth. Ganth. +3",
        legs={ name="Nyame Flanchard", augments={'Path: B',}},
        feet="Leth. Houseaux +3",
        neck="Fotia Gorget",
        waist="Orpheus's Sash",
        left_ear={ name="Moonshade Earring", augments={'Accuracy+4','TP Bonus +250',}},
        right_ear="Malignance Earring",
        left_ring="Weather. Ring",
        right_ring="Cornelia's Ring",
        back={ name="Sucellos's Cape", augments={'MND+20','Mag. Acc+20 /Mag. Dmg.+20','MND+10','Weapon skill damage +10%',}},
    }

    sets.WS['Judgment'] = sets.WS['Savage Blade']
    sets.WS['Black Halo'] = sets.WS['Savage Blade']

    sets.WS['Asuran Fists'] = {
        ammo={ name="Coiste Bodhar", augments={'Path: A',}},
        head="Leth. Chappel +3",
        body="Lethargy Sayon +3",
        hands="Leth. Ganth. +3",
        legs="Leth. Fuseau +3",
        feet="Leth. Houseaux +3",
        neck="Sanctity Necklace",
        waist={ name="Sailfi Belt +1", augments={'Path: A',}},
        left_ear="Telos Earring",
        right_ear={ name="Leth. Earring +1", augments={'System: 1 ID: 1676 Val: 0','Accuracy+11','Mag. Acc.+11','"Dbl.Atk."+3',}},
        left_ring="Cornelia's Ring",
        right_ring="Ilabrat Ring",
        back={ name="Sucellos's Cape", augments={'STR+20','Accuracy+20 Attack+20','STR+10','Weapon skill damage +10%',}},
    }

    sets.WS['Sanguine Blade'] = {
        ammo="Sroda Tathlum",
        neck={ name="Dls. Torque +2", augments={'Path: A',}},
        left_ear="Malignance Earring",
        right_ear="Regal Earring",
        head="Pixie hairpin +1",
        body={ name="Nyame Mail", augments={'Path: B',}},
        hands="Jhakri Cuffs +2",
        left_ring="Archon Ring",
        right_ring="Cornelia's Ring",
        back={ name="Sucellos's Cape", augments={'MND+20','Mag. Acc+20 /Mag. Dmg.+20','MND+10','Weapon skill damage +10%',}},
        waist="Orpheus's Sash",
        legs="Leth. Fuseau +3",
        feet="Leth. Houseaux +3",
    }

    sets.SB = sets.WS['Seraph Blade']

    -- Cap is 82%
    -- 38% from RDM traits
    -- 44% from gear
    sets.FC = {
        ammo="Impatiens",
        head="Atrophy Chapeau +3", -- 16%
        body={ name="Viti. Tabard +3", augments={'Enhances "Chainspell" effect',}}, -- 15%
        waist="Witful Belt", -- 3%
        back="Sucellos's cape", -- 10%
        left_ring="Weather. Ring", -- 5%
    }

    --[[
                    Magic Haste
        DW      0%	10%	15%	30%	Cap
        T1(10)	64	60	57	46	26
        T2(15)	59	55	52	41	21
        T3(25)	49	45	42	31	11
        T4(30)	44	40	37	26	 6
        T5(35)	39	35	32	21	 1

        ** Assuming 25% gear haste
    ]]
    -- With only Haste 2 and /nin we'll need 31 DW from gear
    sets.TP.Enspell = {
        left_ring="Chirich Ring +1",
        main="Esoteric athame",
        sub="Ceremonial Dagger",
        range="Ullr",
        head="Malignance Chapeau", -- 6% Haste
        body="Malignance Tabard", -- 4% Haste
        hands="Aya. Manopolas +2", -- 4% Haste
        legs="Carmine Cuisses +1", -- 6% Haste, 6% DW
        feet="Malignance boots", -- 3% Haste
        neck="Sanctity Necklace",
        waist="Orpheus's Sash",
        left_ear="Eabani Earring", -- 4% DW
        right_ear="Suppanomimi", -- 5% DW
        right_ring="Chirich Ring +1",
        back={ name="Sucellos's Cape", augments={'DEX+20','Accuracy+20 Attack+20','"Dual Wield"+10',}}, -- 10% DW
    }

    sets.TP.Crocea = {
        left_ring="Chirich Ring +1",
        main="Crocea Mors",
        sub="Daybreak",
        range="Ullr",
        head="Malignance Chapeau", -- 6% Haste
        body="Malignance Tabard", -- 4% Haste
        hands="Aya. Manopolas +2", -- 4% Haste
        legs="Carmine Cuisses +1", -- 6% Haste, 6% DW
        feet="Malignance boots", -- 3% Haste
        neck="Sanctity Necklace",
        waist="Orpheus's Sash",
        left_ear="Eabani Earring", -- 4% DW
        right_ear="Suppanomimi", -- 5% DW
        right_ring="Chirich Ring +1",
        back={ name="Sucellos's Cape", augments={'DEX+20','Accuracy+20 Attack+20','"Dual Wield"+10',}}, -- 10% DW
    }

    sets.TP.Standard = {
        main="Naegling",
        sub="Thibron",
        ammo={ name="Coiste Bodhar", augments={'Path: A',}},
        head="Malignance Chapeau",
        body="Malignance Tabard",
        hands="Malignance Gloves",
        legs="Malignance Tights",
        feet="Malignance Boots",
        neck="Anu Torque",
        waist="Reiki Yotai",
        left_ear="Sherida Earring",
        right_ear={ name="Leth. Earring +1", augments={'System: 1 ID: 1676 Val: 0','Accuracy+11','Mag. Acc.+11','"Dbl.Atk."+3',}},
        left_ring="Defending Ring",
        right_ring="Chirich Ring +1",
        back={ name="Sucellos's Cape", augments={'DEX+20','Accuracy+20 Attack+20','"Dual Wield"+10','Damage taken-5%',}},
    }

    sets.TP.Club = set_combine(sets.TP.Standard, {
        main="Maxentius",
        sub="Bunzi's rod"
    })

    sets.TP.Aeolian = set_combine(sets.TP.Standard, {
        main="Tauret",
        sub="Thibron",
    })

    sets.TP.H2H = set_combine(sets.TP.Standard, {
        main="Karambit",
        sub="",
        waist={ name="Sailfi Belt +1", augments={'Path: A',}},
    })

    -- Slow, Para, etc.
    sets.Enfeebling.Potency = {
        main="Daybreak",
        sub="Ammurapi shield",
        ammo="Regal Gem",
        head={ name="Viti. Chapeau +3", augments={'Enfeebling Magic duration','Magic Accuracy',}},
        body="Lethargy sayon +3",
        hands="Leth. Ganth. +3",
        legs="Leth. Fuseau +3",
        feet={ name="Vitiation Boots +3", augments={'Immunobreak Chance',}},
        neck={ name="Dls. Torque +2", augments={'Path: A',}},
        waist="Obstin. Sash",
        left_ear="Regal Earring",
        right_ear="Snotra Earring",
        left_ring={ name="Metamor. Ring +1", augments={'Path: A',}},
        right_ring="Stikini Ring +1",
        back={ name="Sucellos's Cape", augments={'MND+20','Mag. Acc+20 /Mag. Dmg.+20','MND+10','Weapon skill damage +10%',}},
    }

    -- Frazzle, Distract
    sets.Enfeebling.Skill = {
        -- main="Contemplator +1",
        main="Bunzi's Rod",
        sub="Ammurapi shield",
        ammo="Regal gem",
        head={ name="Viti. Chapeau +3", augments={'Enfeebling Magic duration','Magic Accuracy',}},
        body="Atrophy Tabard +3",
        hands="Leth. Ganth. +3",
        legs={ name="Chironic Hose", augments={'Mag. Acc.+29','Haste+1','MND+11',}},
        feet="Leth. Houseaux +3",
        neck="Weike Torque",
        -- neck="Incanter's torque",
        waist="Rumination Sash",
        left_ear="Enfeebling earring",
        right_ear="Vor earring",
        left_ring="Stikini Ring +1",
        right_ring="Stikini Ring +1",
        back={ name="Sucellos's Cape", augments={'INT+20','Mag. Acc+20 /Mag. Dmg.+20','INT+10','"Mag.Atk.Bns."+10',}},
    }

    -- Sleep, Bind, Gravity
    sets.Enfeebling.Duration = {
        main="Bunzi's Rod",
        sub="Ammurapi shield",
        ammo="Regal Gem",
        head={ name="Viti. Chapeau +3", augments={'Enfeebling Magic duration','Magic Accuracy',}},
        body="Atrophy Tabard +3",
        hands="Regal Cuffs",
        legs={ name="Chironic Hose", augments={'Mag. Acc.+29','Haste+1','MND+11',}},
        feet={ name="Vitiation Boots +3", augments={'Immunobreak Chance',}},
        neck={ name="Dls. Torque +2", augments={'Path: A',}},
        waist="Obstin. Sash",
        left_ear="Regal Earring",
        right_ear="Snotra Earring",
        left_ring="Stikini Ring +1",
        right_ring="Kishar Ring",
        back={ name="Sucellos's Cape", augments={'MND+20','Mag. Acc+20 /Mag. Dmg.+20','MND+10','Weapon skill damage +10%',}},
    }

    sets.NukingMode.Standard = {
        main="Bunzi's Rod",
        sub="Ammurapi shield",
        ammo="Ghastly Tathlum +1",
        head="Leth. Chappel +3",
        body="Lethargy sayon +3",
        hands="Leth. Ganth. +3",
        legs="Leth. Fuseau +3",
        feet="Leth. Houseaux +3",
        neck="Sibyl Scarf",
        waist="Acuity belt +1",
        left_ear="Malignance Earring",
        right_ear="Regal Earring",
        left_ring={ name="Metamor. Ring +1", augments={'Path: A',}},
        right_ring="Freke ring",
        back={ name="Sucellos's Cape", augments={'INT+20','Mag. Acc+20 /Mag. Dmg.+20','INT+10','"Mag.Atk.Bns."+10',}},
    }

    sets.NukingMode.MagicBurst = {
        main="Bunzi's Rod",
        sub="Ammurapi shield",
        ammo="Ghastly Tathlum +1",
        head="Ea Hat",
        body="Ea Houppelande",
        hands="Regal Cuffs",
        legs="Leth. Fuseau +3",
        feet="Leth. Houseaux +3",
        neck="Sibyl Scarf",
        waist="Acuity belt +1",
        left_ear="Malignance Earring",
        right_ear="Regal Earring",
        right_ring="Freke Ring",
        left_ring={ name="Metamor. Ring +1", augments={'Path: A',}},
        back={ name="Sucellos's Cape", augments={'INT+20','Mag. Acc+20 /Mag. Dmg.+20','INT+10','"Mag.Atk.Bns."+10',}},
    }

    sets.EnhancingOthers = {
        main={ name="Colada", augments={'Enh. Mag. eff. dur. +3','CHR+7','"Mag.Atk.Bns."+17','DMG:+14',}},
        sub="Ammurapi shield",
        head="Lethargy Chappel +3",
        body="Lethargy sayon +3",
        hands="Atrophy Gloves +3",
        legs="Leth. Fuseau +3",
        feet="Leth. Houseaux +3",
        neck={ name="Dls. Torque +2", augments={'Path: A',}},
        left_ear="Magnetic Earring",
        right_ear="Lethargy Earring +1",
        waist="Embla Sash",
        back="Ghostfyre Cape",
    }

    sets.EnhancingSelf = {
        main={ name="Colada", augments={'Enh. Mag. eff. dur. +3','CHR+7','"Mag.Atk.Bns."+17','DMG:+14',}},
        sub="Ammurapi shield",
        head="Telchine Cap",
        body={ name="Viti. Tabard +3", augments={'Enhances "Chainspell" effect',}},
        hands="Atrophy Gloves +3",
        legs="Telchine Braconi",
        feet="Leth. Houseaux +3",
        neck={ name="Dls. Torque +2", augments={'Path: A',}},
        left_ear="Magnetic Earring",
        right_ear="Lethargy Earring +1",
        waist="Embla Sash",
        back="Ghostfyre Cape",
    }

    sets.Cure = {
        ammo="Staunch Tathlum +1",
        head={ name="Kaykaus Mitra +1", augments={'MP+80','"Cure" spellcasting time -7%','Enmity-6',}},
        body={ name="Kaykaus Bliaut +1", augments={'MP+80','MND+12','Mag. Acc.+20',}},
        hands={ name="Kaykaus Cuffs +1", augments={'MP+80','MND+12','Mag. Acc.+20',}},
        legs={ name="Kaykaus Tights +1", augments={'MP+80','MND+12','Mag. Acc.+20',}},
        feet={ name="Kaykaus Boots +1", augments={'MP+80','"Cure" spellcasting time -7%','Enmity-6',}},
        neck={ name="Dls. Torque +2", augments={'Path: A',}},
        waist="Plat. Mog. Belt",
        left_ear="Roundel Earring",
        right_ear="Magnetic Earring",
        left_ring="Stikini Ring +1",
        right_ring="Stikini Ring +1",
        back="Moonlight cape"
    }

    -- 404 skill from 99 RDM
    -- +161from gear
    sets.EnhancingSkill = {
        main="Pukulatmuj +1", -- 11
        sub="Ammurapi shield",
        ammo="Staunch Tathlum +1",
        head="Befouled crown", -- 16
        neck="Incanter's Torque", -- 10
        left_ear="Magnetic Earring",
        right_ear="Lethargy Earring +1",
        body={ name="Viti. Tabard +3", augments={'Enhances "Chainspell" effect',}}, -- 23
        hands="Viti. gloves +3", -- 24
        left_ring="Stikini Ring +1", -- 8
        right_ring="Stikini Ring +1", -- 8
        back={ name="Sucellos's Cape", augments={'MND+20','Mag. Acc+20 /Mag. Dmg.+20','Mag. Acc.+10','"Fast Cast"+10',}},
        waist="Olympus Sash", -- 5
        legs="Atrophy Tights +3", -- 21
        feet="Leth. Houseaux +3", -- 30
    }

    sets.Phalanx = {
        main="Sakpata's Sword",
        sub="Egeking",
        ammo="Staunch Tathlum +1",
        head={ name="Taeon Chapeau", augments={'Phalanx +3',}},
        body="Merlinic jubbah",
        hands="Atrophy Gloves +3",
        legs={ name="Taeon Tights", augments={'Phalanx +3',}},
        feet="Leth. Houseaux +3",
        neck={ name="Dls. Torque +2", augments={'Path: A',}},
        waist="Embla Sash",
        left_ear="Genmei Earring",
        right_ear={ name="Leth. Earring +1", augments={'System: 1 ID: 1676 Val: 0','Accuracy+11','Mag. Acc.+11','"Dbl.Atk."+3',}},
        left_ring="Defending Ring",
        back={ name="Sucellos's Cape", augments={'DEX+20','Accuracy+20 Attack+20','"Dual Wield"+10','Damage taken-5%',}},
    }

    sets.Smithing = {
        sub="Smythe's Shield",
        head="Qiqirn Hood",
        body="Blksmith. Smock",
        hands="Smithy's Mitts",
        legs="Carmine Cuisses +1",
        neck="Smithy's Torque",
        left_ring="Craftmaster's Ring",
        right_ring="Craftkeeper's Ring",
    }

    sets.Refresh = {
        body="Atrophy Tabard +3",
        legs="Leth. Fuseau +3",
    }

    sets.Dispel = {
        left_ring="Stikini Ring +1",
        main="Daybreak",
        sub="Ammurapi shield",
        range="Ullr",
        head={ name="Viti. Chapeau +3", augments={'Enfeebling Magic duration','Magic Accuracy',}},
        body="Atrophy Tabard +3",
        hands="Leth. Ganth. +3",
        legs={ name="Chironic Hose", augments={'Mag. Acc.+29','Haste+1','MND+11',}},
        feet={ name="Vitiation Boots +3", augments={'Immunobreak Chance',}},
        neck={ name="Dls. Torque +2", augments={'Path: A',}},
        waist="Rumination Sash",
        left_ear="Malignance Earring",
        right_ear="Snotra Earring",
        back={ name="Sucellos's Cape", augments={'MND+20','Mag. Acc+20 /Mag. Dmg.+20','Mag. Acc.+10','"Fast Cast"+10',}},
        right_ring="Stikini Ring +1",
    }

    sets.Enmity = {
        ammo="Sapience Orb",
        head={ name="Nyame Helm", augments={'Path: B',}},
        body="Emet Harness +1",
        hands="Leth. Ganth. +3",
        legs={ name="Nyame Flanchard", augments={'Path: B',}},
        feet={ name="Nyame Sollerets", augments={'Path: B',}},
        neck={ name="Loricate Torque +1", augments={'Path: A',}},
        waist="Plat. Mog. Belt",
        left_ear="Eabani Earring",
        right_ear="Friomisi Earring",
        left_ring="Petrov Ring",
        right_ring="Begrudging Ring",
        back="Moonlight Cape",
    }

    sets.Aquaveil = set_combine(sets.EnhancingSkill, {
        hands="Regal Cuffs",
    })
end

function precast(spell)
    if spell.action_type == 'Magic' and (spell.recast_id or spell.id) then
        local spell_recasts = windower.ffxi.get_spell_recasts()
        if spell_recasts[spell.recast_id or spell.id] and spell_recasts[spell.recast_id or spell.id] > 0 then
            cancel_spell()
        elseif buffactive['terror'] or buffactive['petrified'] or buffactive['sleep'] or buffactive['stun'] or spell.target.distance > 21 then
            cancel_spell()
        elseif spell.name == "Erase" and player.sub_job_full ~= 'White Mage' then
            send_command('input /item "Panacea" <me>')
        else
            equip(sets.FC)
        end
    elseif spell.type == "WeaponSkill" then
        if buffactive['amnesia'] or spell.target.distance > 6 then
            cancel_spell()
        else
            equip(sets.WS[spell.english])
        end
    end
end

EnhancingSkillSpells = S{'Temper', 'Temper II', 'Barfire', 'Barfira', 'Barblizzard', 'Barblizzara', 'Baraero', 'Baraera', 'Barstone', 'Barstonra', 'Barthunder', 'Barthundra', 'Barwater', 'Barwatera', 'Baramnesia', 'Baramnesra', 'Barvirus', 'Barvira', 'Barparalyze', 'Barparalyzra', 'Barsilence', 'Barsilencera', 'Barpetrify', 'Barpetra', 'Barpoison', 'Barpoisonra', 'Barblind', 'Barblindra', 'Barsleep', 'Barsleepra', 'Gain-AGI', 'Gain-CHR', 'Gain-DEX', 'Gain-INT', 'Gain-MND', 'Gain-STR', 'Gain-VIT', 'Enthunder', 'Enstone', 'Enaero', 'Enblizzard', 'Enfire', 'Enwater', 'Enthunder II', 'Enstone II', 'Enaero II', 'Enblizzard II', 'Enfire II', 'Enwater II', 'Phalanx', 'Phalanx II'}
EnfeeblingDurationSpells = S{'Dia', 'Dia II', 'Dia III', 'Break', 'Bind', 'Gravity II', 'Sleep', 'Sleep II'}
EnfeeblingSkillSpells = S{'Frazzle III', 'Distract III'}
Enmity = S{'Flash'}

function midcast(spell)
    if spell.name == "Absorb-TP" then
        equip(set_combine(sets.Enfeebling.Skill, {
            neck="Erra pendant",
            waist={ name="Sailfi Belt +1", augments={'Path: A',}},
            left_ear="Malignance Earring",
            right_ear="Lethargy Earring +1",
            left_ring="Weather. Ring",
            right_ring="Stikini Ring +1",
            back={ name="Sucellos's Cape", augments={'MND+20','Mag. Acc+20 /Mag. Dmg.+20','Mag. Acc.+10','"Fast Cast"+10',}},
        }))
    elseif spell.skill == 'Enfeebling Magic' then
        if EnfeeblingDurationSpells:contains(spell.name) then
            equip(sets.Enfeebling.Duration)
        elseif spell.english == "Dispel" or spell.english == "Dispelga" then
            equip(sets.Dispel)
        elseif EnfeeblingSkillSpells:contains(spell.name) then
            equip(sets.Enfeebling.Skill)
        else
            equip(sets.Enfeebling.Potency)
        end

        if player.sub_job_full == 'Ninja' or player.sub_job_full == 'Dancer' then
            equip({
                main="Bunzi's Rod",
                sub="Maxentius",
            })
        end

        if buffactive['Saboteur'] then
            equip({ hands="Leth. Ganth. +3" })
        end
    elseif spell.skill == 'Enhancing Magic' then
        if spell.english == "Phalanx" then
            equip(sets.Phalanx)
        elseif EnhancingSkillSpells:contains(spell.name) then
            equip(sets.EnhancingSkill)

            if player.sub_job_full == 'Ninja' or player.sub_job_full == 'Dancer' then
                equip({
                    sub="Secespita"
                })
            end
        elseif spell.target.type == 'SELF' then
            if spell.english == "Refresh III" then
                equip(set_combine(sets.EnhancingSelf, sets.Refresh))
            elseif spell.english == "Aquaveil" then
                equip(sets.Aquaveil)
            else
                equip(sets.EnhancingSelf)
            end
        else
            if spell.english == "Refresh III" then
                equip(set_combine(sets.EnhancingOthers, sets.Refresh))
            else
                equip(sets.EnhancingOthers)
            end
        end
    elseif spell.skill == "Healing Magic" then
        equip(sets.Cure)
    elseif spell.skill == 'Elemental Magic' then
        equip(sets.NukingMode[NukingMode])

        if player.sub_job_full == 'Ninja' or player.sub_job_full == 'Dancer' then
            equip({
                main="Bunzi's Rod",
                sub="Maxentius",
            })
        end
    end
end

function aftercast(spell)
    if player.status == 'Engaged' then
        equip(sets.TP[TPMode])
    else
        equip(sets.Idle)
    end
end

function status_change(new,old)
    if T{'Idle','Resting'}:contains(new) then
        equip(sets.Idle)
    elseif new == 'Engaged' then
        equip(sets.TP[TPMode])
        if player.target.name == "Dhartok" or player.target.name == "Ghatjot" then
            windower.add_to_chat('No physical weaknesses, but weak to Earth')
        end
    end
end

function self_command(command)
    if command == 'TP' then
        if TPMode == "Standard" then
            TPMode = "Enspell"
        elseif TPMode == "Enspell" then
            TPMode = "Crocea"
        elseif TPMode == "Crocea" then
            TPMode = "Aeolian"
        elseif TPMode == "Aeolian" then
            TPMode = "Club"
        elseif TPMode == "Club" then
            TPMode = "H2H"
        elseif TPMode == "H2H" then
            TPMode = "Standard"
        end

        windower.add_to_chat('TP mode is now: '..TPMode)
        equip(sets.TP[TPMode])
    end
    if command == 'NM' then
        if NukingMode =="Standard" then
            NukingMode ="MagicBurst"
        elseif NukingMode =="MagicBurst" then
            NukingMode ="Standard"
        end
        windower.add_to_chat('Nuking mode is now: '..NukingMode)
        equip(sets.NukingMode[NukingMode])
    end
    if command == 'ws' then
        if TPMode == 'Standard' then
            send_command("input /ws 'Savage Blade' <t>")
        elseif TPMode == "Crocea" then
            send_command("input /ws 'Seraph Blade' <t>")
        elseif TPMode == "Aeolian" then
            send_command("input /ws 'Aeolian Edge' <t>")
        elseif TPMode == "Club" then
            send_command("input /ws 'Black Halo' <t>")
        elseif TPMode == "H2H" then
            send_command("input /ws 'Asuran Fists' <t>")
        end
    end
end

Zone_change_delay = 5
windower.raw_register_event('zone change', function (new_id)
    local zones = gearswap.res.zones
    local new_zone = zones[new_id].name
    coroutine.schedule(gearswap.equip_sets:prepare('zone_change', nil, new_zone),(Zone_change_delay))
end)

function zone_change(zoneName)
    if zoneName:endswith('Adoulin') then
        equip({ body="Councilor's Garb" })
    end
end

function user_unload()
    send_command('unbind numpad1')
    send_command('unbind numpad2')
end
