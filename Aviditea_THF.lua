include('organizer-lib')

-- Gear Sets
function get_sets()
    sets.TP = {
        main="Voluspa Knife",
        sub="Gleti's Knife",
        ammo="Hasty Pinion +1",
        head="Gleti's Mask",
        body="Gleti's Cuirass",
        hands="Gleti's Gauntlets",
        legs="Gleti's Breeches",
        feet="Gleti's Boots",
        neck={ name="Loricate Torque +1", augments={'Path: A',}},
        waist="Sailfi Belt +1",
        left_ear="Eabani Earring",
        right_ear={ name="Skulker's Earring", augments={'System: 1 ID: 1676 Val: 0','Accuracy+8','Mag. Acc.+8',}},
        right_ring="Defending Ring",
    }
end

