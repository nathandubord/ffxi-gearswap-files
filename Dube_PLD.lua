function get_sets()
    send_command('bind numpad1 gs c TP')

    TPMode = 'Standard'

    sets.Idle = {
    ammo="Staunch Tathlum +1",
    head="Chev. Armet +2",
    body={ name="Sakpata's Plate", augments={'Path: A',}},
    hands={ name="Souv. Handsch. +1", augments={'HP+65','Shield skill +15','Phys. dmg. taken -4',}},
    legs="Chev. Cuisses +2",
    feet={ name="Souveran Schuhs +1", augments={'HP+105','Enmity+9','Potency of "Cure" effect received +15%',}},
    neck="Combatant's Torque",
    waist="Plat. Mog. Belt",
    left_ear="Thureous Earring",
    right_ear={ name="Chev. Earring", augments={'System: 1 ID: 1676 Val: 0','Accuracy+6','Mag. Acc.+6',}},
    left_ring="Shneddick Ring",
    right_ring="Moonlight Ring",
    back={ name="Rudianos's Mantle", augments={'HP+60','Eva.+20 /Mag. Eva.+20','Mag. Evasion+10','Enmity+10','Chance of successful block +5',}},
}

    sets.WS = {}
    sets.WS['Savage Blade'] = {
    head={ name="Nyame Helm", augments={'Path: B',}},
    body={ name="Nyame Mail", augments={'Path: B',}},
    hands={ name="Nyame Gauntlets", augments={'Path: B',}},
    legs={ name="Nyame Flanchard", augments={'Path: B',}},
    feet={ name="Nyame Sollerets", augments={'Path: B',}},
    neck="Rep. Plat. Medal",
    waist={ name="Sailfi Belt +1", augments={'Path: A',}},
    left_ear="Thrud Earring",
    right_ear={ name="Moonshade Earring", augments={'Accuracy+4','TP Bonus +250',}},
    left_ring="Cornelia's Ring",
    right_ring="Sroda Ring",
    back={ name="Rudianos's Mantle", augments={'STR+20','Accuracy+20 Attack+20','STR+8','Weapon skill damage +10%','Damage taken-5%',}},
	}
	
	sets.WS['Circle Blade'] = {
    ammo="Oshasha's Treatise",
    head={ name="Nyame Helm", augments={'Path: B',}},
    body={ name="Nyame Mail", augments={'Path: B',}},
    hands={ name="Nyame Gauntlets", augments={'Path: B',}},
    legs={ name="Nyame Flanchard", augments={'Path: B',}},
    feet={ name="Nyame Sollerets", augments={'Path: B',}},
    neck="Rep. Plat. Medal",
    waist={ name="Sailfi Belt +1", augments={'Path: A',}},
    left_ear="Thrud Earring",
    right_ear={ name="Moonshade Earring", augments={'Accuracy+4','TP Bonus +250',}},
    left_ring="Cornelia's Ring",
    right_ring="Sroda Ring",
    back={ name="Rudianos's Mantle", augments={'HP+60','Eva.+20 /Mag. Eva.+20','Mag. Evasion+10','Enmity+10','Chance of successful block +5',}},
	}
	

    -- Cap is 80%
    sets.FC = {
ammo="Staunch Tathlum +1",
    head={ name="Souv. Schaller +1", augments={'HP+105','Enmity+9','Potency of "Cure" effect received +15%',}},
    body={ name="Sakpata's Plate", augments={'Path: A',}},
    hands={ name="Souv. Handsch. +1", augments={'HP+65','Shield skill +15','Phys. dmg. taken -4',}},
    legs="Carmine Cuisses +1",
    feet={ name="Odyssean Greaves", augments={'"Waltz" potency +6%','"Cure" spellcasting time -9%','Accuracy+8 Attack+8','Mag. Acc.+7 "Mag.Atk.Bns."+7',}},
    neck="Moonlight Necklace",
    waist={ name="Sailfi Belt +1", augments={'Path: A',}},
    left_ear="Thureous Earring",
    right_ear={ name="Chev. Earring", augments={'System: 1 ID: 1676 Val: 0','Accuracy+6','Mag. Acc.+6',}},
    left_ring="Kishar Ring",
    right_ring="Naji's Loop",
    back={ name="Rudianos's Mantle", augments={'STR+20','Accuracy+20 Attack+20','STR+8','Weapon skill damage +10%','Damage taken-5%',}},
    }

    sets.TP = {}
    sets.TP.Standard = {
	main="",
    sub="",
    ammo={ name="Coiste Bodhar", augments={'Path: A',}},
    head={ name="Sakpata's Helm", augments={'Path: A',}},
    body="Hjarrandi Breast.",
    hands={ name="Sakpata's Gauntlets", augments={'Path: A',}},
    legs={ name="Nyame Flanchard", augments={'Path: B',}},
    feet={ name="Nyame Sollerets", augments={'Path: B',}},
    neck="Combatant's Torque",
    waist={ name="Sailfi Belt +1", augments={'Path: A',}},
    left_ear="Cessance Earring",
    right_ear="Telos Earring",
    left_ring="Chirich Ring +1",
    right_ring="Moonlight Ring",
    back={ name="Rudianos's Mantle", augments={'DEX+20','Accuracy+20 Attack+20','Accuracy+10','"Dbl.Atk."+10','Damage taken-5%',}},
	}

    sets.TP.Turtle = {
	 ammo="Staunch Tathlum +1",
    head="Chev. Armet +2",
    body={ name="Sakpata's Plate", augments={'Path: A',}},
    hands={ name="Souv. Handsch. +1", augments={'HP+65','Shield skill +15','Phys. dmg. taken -4',}},
    legs="Chev. Cuisses +2",
    feet="Reverence leggings +2",
    neck="Loricate Torque +1",
    waist="Sailfi belt +1",
    left_ear="Thureous Earring",
    right_ear={ name="Chev. Earring", augments={'System: 1 ID: 1676 Val: 0','Accuracy+6','Mag. Acc.+6',}},
    left_ring="Chirich Ring +1",
    right_ring="Moonlight Ring",
    back={ name="Rudianos's Mantle", augments={'DEX+20','Accuracy+20 Attack+20','Accuracy+10','"Dbl.Atk."+10','Damage taken-5%',}},
    }

    sets.Cure = {
    ammo="Staunch Tathlum +1",
    head={ name="Souv. Schaller +1", augments={'HP+105','Enmity+9','Potency of "Cure" effect received +15%',}},
    body={ name="Souv. Cuirass +1", augments={'HP+105','Enmity+9','Potency of "Cure" effect received +15%',}},
    hands={ name="Souv. Handsch. +1", augments={'HP+65','Shield skill +15','Phys. dmg. taken -4',}},
    legs={ name="Sakpata's Cuisses", augments={'Path: A',}},
    feet={ name="Souveran Schuhs +1", augments={'HP+105','Enmity+9','Potency of "Cure" effect received +15%',}},
    neck={ name="Loricate Torque +1", augments={'Path: A',}},
    waist={ name="Sailfi Belt +1", augments={'Path: A',}},
    left_ear="Telos Earring",
    right_ear="Crep. Earring",
    left_ring="Naji's Loop",
    right_ring="Kishar Ring",
    back={ name="Rudianos's Mantle", augments={'HP+60','Eva.+20 /Mag. Eva.+20','Mag. Evasion+10','Enmity+10','Chance of successful block +5',}},
	}

    sets.Phalanx = {   
	main="",
    sub="",
    ammo="",
    head={ name="Yorium Barbuta", augments={'Phalanx +3',}},
    body={ name="Yorium Cuirass", augments={'Evasion+20','Spell interruption rate down -8%','Phalanx +3',}},
    hands={ name="Souv. Handsch. +1", augments={'HP+65','Shield skill +15','Phys. dmg. taken -4',}},
    legs={ name="Sakpata's Cuisses", augments={'Path: A',}},
    feet={ name="Souveran Schuhs +1", augments={'HP+105','Enmity+9','Potency of "Cure" effect received +15%',}},
    neck="Incanter's Torque",
    waist="Plat. Mog. Belt",
    left_ear="Thureous Earring",
    right_ear="Odnowa Earring +1",
    left_ring="Kishar Ring",
    right_ring="Naji's Loop",
    back={ name="Weard Mantle", augments={'VIT+2','DEX+4','Enmity+3','Phalanx +2',}},
    }

    sets.Enmity = {
    ammo="Staunch Tathlum +1",
    head={ name="Souv. Schaller +1", augments={'HP+105','Enmity+9','Potency of "Cure" effect received +15%',}},
    body={ name="Souv. Cuirass +1", augments={'HP+105','Enmity+9','Potency of "Cure" effect received +15%',}},
    hands={ name="Souv. Handsch. +1", augments={'HP+65','Shield skill +15','Phys. dmg. taken -4',}},
    legs={ name="Sakpata's Cuisses", augments={'Path: A',}},
    feet={ name="Souveran Schuhs +1", augments={'HP+105','Enmity+9','Potency of "Cure" effect received +15%',}},
    neck="Combatant's Torque",
    waist="Plat. Mog. Belt",
    left_ear="Thureous Earring",
    right_ear="Odnowa Earring +1",
    left_ring="Petrov Ring",
    right_ring="Defending Ring",
    back={ name="Rudianos's Mantle", augments={'HP+60','Eva.+20 /Mag. Eva.+20','Mag. Evasion+10','Enmity+10','Chance of successful block +5',}},
	}

    sets.JA = {}
    sets.JA.Rampart = {
	head="Caballarius coronet +1"
    }
end

function precast(spell)
    if spell.action_type == 'Magic' and (spell.recast_id or spell.id) then
        local spell_recasts = windower.ffxi.get_spell_recasts()
        if spell_recasts[spell.recast_id or spell.id] and spell_recasts[spell.recast_id or spell.id] > 0 then
            cancel_spell()
        elseif buffactive['terror'] or buffactive['petrified'] or buffactive['sleep'] or buffactive['stun'] or spell.target.distance > 21 then
            cancel_spell()
        else
            equip(sets.FC)
        end
    elseif spell.type == "WeaponSkill" then
        if buffactive['amnesia'] or buffactive['terror'] or buffactive['petrified'] or buffactive['sleep'] or buffactive['stun'] or spell.target.distance > 6 then
            cancel_spell()
        else
            equip(sets.WS[spell.english])
        end
    elseif spell.type == "JobAbility" then
        if sets.JA[spell.english] then
            equip(sets.JA[spell.english])
        else
            equip(sets.Enmity)
        end
    end
end

Enmity = S{'Flash'}

function midcast(spell)
    if spell.english == "Phalanx" then
        equip(sets.Phalanx)
    elseif spell.skill == "Healing Magic" then
        equip(sets.Cure)
        end
end

function aftercast(spell)
    if player.status == 'Engaged' then
        equip(sets.TP[TPMode])
    else
        equip(sets.Idle)
    end
end

function status_change(new)
    if T{'Idle','Resting'}:contains(new) then
        equip(sets.Idle)
    elseif new == 'Engaged' then
        equip(sets.TP[TPMode])
    end
end

function self_command(command)
    if command == 'TP' then
        if TPMode == "Standard" then
            TPMode = "Turtle"
        elseif TPMode == "Turtle" then
            TPMode = "Standard"
        end
        windower.add_to_chat('TP mode is now: '..TPMode)
        equip(sets.TP[TPMode])
    end
end

Zone_change_delay = 5
windower.raw_register_event('zone change', function (new_id)
    local zones = gearswap.res.zones
    local new_zone = zones[new_id].name
    coroutine.schedule(gearswap.equip_sets:prepare('zone_change', nil, new_zone),(Zone_change_delay))
end)

function zone_change(zoneName)
    if zoneName:endswith('Adoulin') then
        equip({ body="Councilor's Garb" })
    end
end

function user_unload()
    send_command('unbind numpad1')
end
