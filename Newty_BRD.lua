function get_sets()
    send_command('bind numpad1 gs c TP')

    TPMode = 'Sword'

    sets.Idle = {
        ammo="Staunch Tathlum +1",
        head="Fili Calot +3",
        body="Inyanga jubbah +2",
        hands="Fili Manchettes +2",
        legs="Fili rhingrave +2",
        feet="Fili Cothurnes +3",
        neck={ name="Warder's Charm +1", augments={'Path: A',}},
        waist="Carrier's sash",
        left_ear="Hearty Earring",
        right_ear="Eabani Earring",
        left_ring="Defending Ring",
        right_ring="Inyanga Ring",
        back="Moonlight Cape",
    }

    sets.TP = {}

    --[[
                    Magic Haste
        DW      0%	10%	15%	30%	Cap
        T1(10)	64	60	57	46	26
        T2(15)	59	55	52	41	21
        T3(25)	49	45	42	31	11
        T4(30)	44	40	37	26	 6
        T5(35)	39	35	32	21	 1

        ** Assuming 25% gear haste
    ]]
    -- We should almost always be magic haste capped on bard, meaning we need 11 DW /nin and 21 DW /dnc
    sets.TP.Sword = {
        main="Naegling",
        sub="Centovente",
        head={ name="Nyame Helm", augments={'Path: B',}},
        ammo="Coiste Bodhar",
        body="Ayanmo corazza +2",
        hands="Bunzi's gloves",
        legs={ name="Nyame Flanchard", augments={'Path: B',}},
        feet="Volte spats",
        neck={ name="Bard's Charm +2", augments={'Path: A',}},
        waist="Sailfi belt +1",
        left_ear="Telos Earring",
        right_ear="Cessance Earring",
        left_ring="Chirich Ring +1",
        right_ring="Moonlight Ring",
        back={ name="Intarabus's Cape", augments={'DEX+20','Accuracy+20 Attack+20','DEX+10','"Dual Wield"+10','Damage taken-5%',}}, -- 10%
    }

    sets.TP.Dagger = set_combine(sets.TP.Sword, {
        main="Tauret",
        sub="Centovente",
    })

    sets.WS = {}
    sets.WS['Savage Blade'] = {
        ammo="Oshasha's Treatise",
        head={ name="Nyame Helm", augments={'Path: B',}},
        body={ name="Nyame Mail", augments={'Path: B',}},
        hands={ name="Nyame Gauntlets", augments={'Path: B',}},
        legs={ name="Nyame Flanchard", augments={'Path: B',}},
        feet={ name="Nyame Sollerets", augments={'Path: B',}},
        neck={ name="Bard's Charm +2", augments={'Path: A',}},
        waist={ name="Sailfi Belt +1", augments={'Path: A',}},
        left_ear={ name="Moonshade Earring", augments={'Accuracy+4','TP Bonus +250',}},
        right_ear="Ishvara Earring",
        left_ring="Sroda Ring",
        right_ring="Cornelia's Ring",
        back={ name="Intarabus's Cape", augments={'STR+20','Accuracy+20 Attack+20','STR+10','Weapon skill damage +10%',}},
    }

    sets.WS['Aeolian Edge'] = {
        ammo={ name="Ghastly Tathlum +1", augments={'Path: A',}},
        head={ name="Nyame Helm", augments={'Path: B',}},
        body={ name="Nyame Mail", augments={'Path: B',}},
        hands={ name="Nyame Gauntlets", augments={'Path: B',}},
        legs={ name="Nyame Flanchard", augments={'Path: B',}},
        feet={ name="Nyame Sollerets", augments={'Path: B',}},
        neck="Sibyl Scarf",
        waist="Orpheus's Sash",
        left_ear={ name="Moonshade Earring", augments={'Accuracy+4','TP Bonus +250',}},
        right_ear="Friomisi Earring",
        left_ring="Sroda Ring",
        right_ring="Cornelia's Ring",
        back={ name="Intarabus's Cape", augments={'STR+20','Accuracy+20 Attack+20','STR+10','Weapon skill damage +10%',}},
    }

    -- 80% cap
    -- 77% Current
    sets.FC = {
        range="Blurred Harp +1",
        head="Fili Calot +3", -- 16%
        body="Inyanga jubbah +2", -- 14%
        legs={ name="Kaykaus Tights +1", augments={'MP+80','MND+12','Mag. Acc.+20',}}, -- 7%
        neck="Orunmila's Torque", -- 5%
        waist="Embla Sash", -- 5%
        left_ear="Loquac. Earring", -- 2%
        left_ring="Kishar Ring", -- 4%
        right_ring="Weather. Ring", -- 5%
        back={ name="Intarabus's Cape", augments={'CHR+20','Mag. Acc+20 /Mag. Dmg.+20','"Fast Cast"+10',}}, -- 10%
        feet="Fili Cothurnes +3" -- 13%
    }

    sets.midcast = {}
    sets.midcast.Sleep = {
        range="Blurred Harp +1",
        head="Inyanga Tiara +2",
        body="Fili Hongreline +2",
        hands="Inyan. Dastanas +2",
        legs="Inyanga Shalwar +2",
        feet="Bihu Slippers +3",
        neck="Mnbw. Whistle +1",
        waist={ name="Acuity Belt +1", augments={'Path: A',}},
        left_ear="Crep. Earring",
        right_ear="Regal Earring",
        left_ring="Stikini Ring +1",
        right_ring="Stikini Ring +1", 
    }

    sets.midcast.Songs = {
        head="Fili Calot +3",
        body="Fili Hongreline +2",
        hands="Fili Manchettes +2",
        legs="Inyanga Shalwar +2",
        feet="Brioso Slippers +3",
        neck="Mnbw. Whistle +1",
    }

    sets.midcast.Debuff = {
        main="Tauret",
        sub="Ammurapi shield",
        head="Inyanga Tiara +2",
        neck="Mnbw. Whistle +1",
        left_ear="Rega, Earring",
        right_ear="Gersemi Earring",
        body="Fili Hongreline +2",
        hands="Fili Manchettes +2",
        left_ring="Metamor. Ring +1",
        right_ring="Stikini Ring +1",
        waist="Acuity Belt +1",
        legs="Fili rhingrave +2",
        feet="Brioso Slippers +3",
    }

    sets.midcast.Enhancing = {
        main="Pukulatmuj +1",
        sub="Ammurapi shield",
        neck="Incanter's Torque",
        head="Telchine Cap",
        body="Telchine Chas.",
        hands="Telchine Gloves",
        legs="Telchine Braconi",
        feet="Telchine Pigaches",
        left_ring="Stikini Ring +1",
        right_ring="Stikini Ring +1",
        waist="Embla Sash",
        left_ear="Magnetic Earring",
    }

    sets.midcast.Cure = {
        main="Daybreak",
        sub="Ammurapi shield",
        right_ear="Magnetic Earring",
        head={ name="Kaykaus Mitra +1", augments={'MP+80','"Cure" spellcasting time -7%','Enmity-6',}},
        body={ name="Kaykaus Bliaut +1", augments={'MP+80','MND+12','Mag. Acc.+20',}},
        hands={ name="Kaykaus Cuffs +1", augments={'MP+80','MND+12','Mag. Acc.+20',}},
        legs={ name="Kaykaus Tights +1", augments={'MP+80','MND+12','Mag. Acc.+20',}},
        feet={ name="Kaykaus Boots +1", augments={'MP+80','"Cure" spellcasting time -7%','Enmity-6',}},
        left_ring="Stikini Ring +1",
        right_ring="Stikini Ring +1",
        waist="Plat. Mog. Belt",
    }
end

function precast(spell)
    if spell.action_type == 'Magic' and (spell.recast_id or spell.id) then
        if spell.name == 'Honor March' then
            equip(set_combine(sets.FC, { range="Marsyas" }))
        else
            equip(sets.FC)
        end
    elseif spell.type == "WeaponSkill" then
        if buffactive['amnesia'] or spell.target.distance > 6 then
            cancel_spell()
        else
            equip(sets.WS[spell.english])
        end
    end
end

function midcast(spell)
    if spell.cast_time then
        if spell.english:contains("Lullaby") then
            equip(sets.midcast.Sleep)
        elseif spell.skill == 'Enfeebling Magic' or spell.english == "Magic Finale" or spell.english:contains('Elegy') or spell.english:contains('Threnody') or spell.english:contains('Requiem') or spell.english == "Pining Nocturne" then
            equip(sets.midcast.Debuff)
        elseif spell.skill == "Enhancing Magic" then
            equip(sets.midcast.Enhancing)
        else
            if (spell.english:contains("Scherzo")) then
                equip(sets.combine(sets.midcast.Songs, { feet="Fili Cothurnes +3" }))
            else
                equip(sets.midcast.Songs)
            end
        end
    end
end

function aftercast(spell)
    if player.status == 'Engaged' then
        equip(sets.TP[TPMode])
    else
        equip(sets.Idle)
    end
end

function status_change(new, old)
    if new == 'Engaged' then
        equip(sets.TP[TPMode])
    else
        equip(sets.Idle)
    end
end

function self_command(command)
    if command == 'TP' then
        if TPMode == "Sword" then
            TPMode = "Dagger"
        elseif TPMode == "Dagger" then
            TPMode = "Sword"
        end
        windower.add_to_chat('TP mode is now: '..TPMode)
        equip(sets.TP[TPMode])
    end
    if command == 'ws' then
        if TPMode == 'Sword' then
            send_command("input /ws 'Savage Blade' <t>")
        elseif TPMode == "Dagger" then
            send_command("input /ws 'Aeolian Edge' <t>")
        end
    end
end