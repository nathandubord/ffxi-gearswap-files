include('organizer-lib')

function get_sets()
  sets.Idle = {
    main="Naegling",
    sub="Skinflayer",
    range="",
    head="Fili Calot +3",
    body="Ashera harness",
    hands="Fili Manchettes +2",
    legs="Bunzi's Pants",
    feet="Fili Cothurnes +2",
    neck="Loricate Torque +1",
    waist="Eschan Stone",
    left_ear="Eabani Earring",
    right_ear="Fili Earring",
    left_ring="Ayanmo Ring",
    right_ring="Inyanga Ring",
    back={ name="Intarabus's Cape", augments={'STR+20','Accuracy+20 Attack+20','STR+5','Weapon skill damage +10%','Phys. dmg. taken-10%',}},
}

  -- 87%
  sets.FC = {
    main={ name="Kali", augments={'Mag. Acc.+15','String instrument skill +10','Wind instrument skill +10',}}, -- 7%
    range="Daurdabla",
	  head="Fili Calot +3", -- 16%
    body="Inyanga Jubbah +2", -- 14%
    hands={ name="Gende. Gages +1", augments={'Phys. dmg. taken -2%','Song spellcasting time -3%',}}, -- 10%
    legs="Aya. Cosciales +2", -- 6%
    waist="Aoidos' Belt", -- 3%
    left_ear="Aoidos' Earring", -- 2%
	  left_ring="Weather. Ring", -- 5%
    right_ring="Kishar Ring", -- 4%
    back={ name="Intarabus's Cape", augments={'CHR+20','Mag. Acc+20 /Mag. Dmg.+20','"Fast Cast"+10','Spell interruption rate down-10%',}}, -- 10%
    feet="Fili Cothurnes +2", -- 10%
  }

  sets.Songs = {
    main="Kali",
    range="Daurdabla",
    head="Fili calot +3",
    neck="Moonbow whistle +1",
    body="Fili hongreline +3",
    hands="Fili manchettes +2",
    back={ name="Intarabus's Cape", augments={'CHR+20','Mag. Acc+20 /Mag. Dmg.+20','"Fast Cast"+10','Spell interruption rate down-10%',}},
    legs="Inyanga Shalwar +2",
    feet="Brioso slippers +3",
  }

  sets.Sleep = {
    main={ name="Kali", augments={'Mag. Acc.+15','String instrument skill +10','Wind instrument skill +10',}},
    sub="Culminus",
    range="Daurdabla",
    head="Fili Calot +3",
    body="Fili Hongreline +3",
    hands="Inyan. Dastanas +2",
    legs="Inyanga Shalwar +2",
    feet="Fili Cothurnes +2",
    neck="Mnbw. Whistle +1",
    waist="Eschan Stone",
    left_ear="Gersemi Earring",
    right_ear={ name="Fili Earring", augments={'System: 1 ID: 1676 Val: 0','Accuracy+9','Mag. Acc.+9',}},
    left_ring="Weather. Ring",
    right_ring="Inyanga Ring",
    back={ name="Intarabus's Cape", augments={'CHR+20','Mag. Acc+20 /Mag. Dmg.+20','"Fast Cast"+10','Spell interruption rate down-10%',}}, 
  }

  sets.TP = {
    main="Naegling",
    range={ name="Linos", augments={'Accuracy+15','Weapon skill damage +3%','STR+6 AGI+6',}},
    head="Aya. Zucchetto +2",
    body="Ashera Harness",
    hands="Bunzi's Gloves",
    legs={ name="Telchine Braconi", augments={'Accuracy+20','"Dbl.Atk."+3','Weapon skill damage +3%',}},
    feet={ name="Telchine Pigaches", augments={'Accuracy+15 Attack+15','"Dbl.Atk."+3','Weapon skill damage +3%',}},
    neck={ name="Bard's Charm +1", augments={'Path: A',}},
    waist="Sailfi Belt +1",
    left_ear="Eabani Earring",
    right_ear="Cessance Earring",
    left_ring="Ilabrat Ring",
    right_ring="Petrov Ring",
    back={ name="Intarabus's Cape", augments={'DEX+20','Accuracy+20 Attack+20','Accuracy+9','"Dbl.Atk."+10',}},
}

  sets.WS = {}
  sets.WS["Savage Blade"] = {
    main="Naegling",
    sub={ name="Skinflayer", augments={'Weapon skill damage +3%','STR+2','Attack+2','DMG:+10',}},
    range={ name="Linos", augments={'Accuracy+15','Weapon skill damage +3%','STR+6 AGI+6',}},
    head="Aya. Zucchetto +2",
    body={ name="Bihu Jstcorps. +3", augments={'Enhances "Troubadour" effect',}},
    hands="Bunzi's Gloves",
    legs={ name="Telchine Braconi", augments={'Accuracy+20','"Dbl.Atk."+3','Weapon skill damage +3%',}},
    feet={ name="Telchine Pigaches", augments={'Accuracy+15 Attack+15','"Dbl.Atk."+3','Weapon skill damage +3%',}},
    neck={ name="Bard's Charm +1", augments={'Path: A',}},
    waist="Sailfi Belt +1",
    left_ear="Eabani Earring",
    right_ear="Ishvara Earring",
    left_ring="Ilabrat Ring",
    right_ring="Apate Ring",
    back={ name="Intarabus's Cape", augments={'STR+20','Accuracy+20 Attack+20','STR+5','Weapon skill damage +10%','Phys. dmg. taken-10%',}},
  }
end


function precast(spell)
  if spell.cast_time then
    equip(sets.FC)
    if spell.name == 'Honor March' then
      equip({ range="Marsyas", })
    end
  end
end

function midcast(spell)
  if spell.cast_time then
    if spell.english:contains("Lullaby") then
      equip(sets.Sleep)

      if player.sub_job_full == 'Ninja' or player.sub_job_full == 'Dancer' then
        equip({
            sub="Naegling"
        })
      end
    elseif spell.name == 'Honor March' then
      equip(set_combine(sets.Songs, { range="Marsyas", }))

      if player.sub_job_full == 'Ninja' or player.sub_job_full == 'Dancer' then
        equip({
            sub="Legato Dagger"
        })
      end
    else
      equip(sets.Songs)

      if player.sub_job_full == 'Ninja' or player.sub_job_full == 'Dancer' then
        equip({
            sub="Legato Dagger"
        })
      end
    end
  elseif sets.WS[spell.english] then
    equip(sets.WS[spell.english])
  elseif spell.english == "Troubadour" then
    equip({
      body="Bihu justaucorps +3",
    })
  end
end

function aftercast(spell)
    if player.status == 'Engaged' then
        equip(sets.TP)
    else
        equip(sets.Idle)
    end
end

function status_change(new,old)
    if T{'Idle','Resting'}:contains(new) then
        equip(sets.Idle)
    elseif new == 'Engaged' then
        equip(sets.TP)
    end
end
