function get_sets()
    -- Set macro book when changing jobs (sheet, book)
    windower.send_command('input /macro book ' .. tostring(10) .. '; wait 0.1; input /macro set ' .. tostring(3))

    Weapon = 'Sword'

    sets.Idle = {
        ammo="Staunch Tathlum +1",
        head="Hjarrandi Helm",
        body="Hjarrandi Breast.",
        hands={ name="Nyame Gauntlets", augments={'Path: B',}},
        legs={ name="Nyame Flanchard", augments={'Path: B',}},
        feet={ name="Nyame Sollerets", augments={'Path: B',}},
        neck={ name="Loricate Torque +1", augments={'Path: A',}},
        waist={ name="Sailfi Belt +1", augments={'Path: A',}},
        left_ear="Telos Earring",
        right_ear={ name="Boii Earring +1", augments={'System: 1 ID: 1676 Val: 0','Accuracy+13','Mag. Acc.+13','Crit.hit rate+4',}},
        left_ring="Moonlight Ring",
        right_ring="Shneddick Ring",
        back="Shadow Mantle",
    }

    TPMode = 'TP'
    sets.TP = {}
    sets.TP.TP = {
        ammo={ name="Coiste Bodhar", augments={'Path: A',}},
        head="Boii Mask +3",
        body="Hjarrandi Breast.",
        hands={ name="Sakpata's Gauntlets", augments={'Path: A',}},
        legs="Pumm. Cuisses +3",
        feet="Pumm. Calligae +3",
        neck={ name="War. Beads +2", augments={'Path: A',}},
        waist={ name="Sailfi Belt +1", augments={'Path: A',}},
        left_ear="Telos Earring",
        right_ear={ name="Boii Earring +1", augments={'System: 1 ID: 1676 Val: 0','Accuracy+13','Mag. Acc.+13','Crit.hit rate+4',}},
        left_ring="Petrov Ring",
        right_ring="Niqmaddu Ring",
        back={ name="Cichol's Mantle", augments={'DEX+20','Accuracy+20 Attack+20','DEX+10','"Dbl.Atk."+10','Damage taken-5%',}},
    }

	sets.TP.TwoHanded = {
        ammo={ name="Coiste Bodhar", augments={'Path: A',}},
        head="Boii Mask +3",
        body="Boii Lorica +3",
        hands={ name="Sakpata's Gauntlets", augments={'Path: A',}},
        legs="Pumm. Cuisses +3",
        feet="Pumm. Calligae +3",
        neck={ name="War. Beads +2", augments={'Path: A',}},
        waist={ name="Sailfi Belt +1", augments={'Path: A',}},
        left_ear="Telos Earring",
        right_ear={ name="Boii Earring +1", augments={'System: 1 ID: 1676 Val: 0','Accuracy+13','Mag. Acc.+13','Crit.hit rate+4',}},
        left_ring="Petrov Ring",
        right_ring="Niqmaddu Ring",
        back={ name="Cichol's Mantle", augments={'DEX+20','Accuracy+20 Attack+20','DEX+10','"Dbl.Atk."+10','Damage taken-5%',}},
    }


	sets.TP.Hybrid = {
        ammo="Staunch Tathlum",
        head="Sakpata's Helm",
        body="Sakpata's Plate",
        hands={ name="Sakpata's Gauntlets", augments={'Path: A',}},
        legs="Sakpata's Cuisses",
        feet="Sakpata's Leggings",
        neck={ name="War. Beads +2", augments={'Path: A',}},
        waist={ name="Sailfi Belt +1", augments={'Path: A',}},
        left_ear="Telos Earring",
        right_ear="Boii Earring +1",
        left_ring="Defending Ring",
        right_ring="Niqmaddu Ring",
        back={ name="Cichol's Mantle", augments={'DEX+20','Accuracy+20 Attack+20','DEX+10','"Dbl.Atk."+10','Damage taken-5%',}},
    }

    -- Job ability sets
    sets.JA = {}
    sets.JA.Aggressor = {
        head="Pummeler's Mask +2",
        body="Agoge Lorica +3"
    }

    sets.JA.Warcry = {
        head="Agoge mask +3"
    }

	sets.JA.Berserk = {
        feet="Agoge calligae +3",
		body="Pummeler's lorica +3"
    }
    
    sets.JA.Restraint = {
        hands="Boii Mufflers +3"
    }

    sets.JA.Retaliation = {
        feet="Boii calligae +2",
		hands="Pummeler's mufflers +2"
    }

    sets.JA['Blood Rage'] = {
        body="Boii Lorica +3"
    }

    sets.JA['Mighty Strikes'] = {
        hands="Agoge mufflers +3"
    }

    sets.JA.Tomahawk = {
        ammo="Thr. Tomahawk",
        feet="Agoge Calligae +1"
    }

    -- Weaponskill sets
    sets.WS = {}
    sets.WS["Savage Blade"] = {
        ammo="knobkierrie",
        head="Agoge mask +3",
        neck="Warrior's bead necklace +2",
        ear1="Thrud earring",
        ear2="Moonshade earring",
        body="Nyame Mail",
        hands="Boii mufflers +3",
        ring1="Cornelia's Ring",
        ring2="Sroda Ring",
        back={ name="Cichol's Mantle", augments={'STR+20','Accuracy+20 Attack+20','STR+10','Weapon skill damage +10%',}},
        waist="Sailfi belt +1",
        legs="Boii cuisses +3",
        feet="Nyame sollerets"
    }

    sets.WS["Requiescat"] = {
        ammo="knobkierrie",
        head="Agoge mask +3",
        neck="Fotia gorget",
        ear1="Thrud earring",
        ear2="Moonshade earring",
        body="Nyame Mail",
        hands="Boii mufflers +3",
        ring1="Cornelia's Ring",
        ring2="Sroda Ring",
        back={ name="Cichol's Mantle", augments={'STR+20','Accuracy+20 Attack+20','STR+10','Weapon skill damage +10%',}},
        waist="Fotia Belt",
        legs="Boii cuisses +3",
        feet="Nyame sollerets"
    }
	
	sets.WS["Circle Blade"] = {
        ammo="knobkierrie",
        head="Agoge mask +3",
        neck="Fotia gorget",
        ear1="Cessance earring",
        ear2="Moonshade earring",
        body="Nyame Mail",
        hands="Sakpata's gauntlets",
        ring1="Flamma Ring",
        ring2="Petrov Ring",
        back={ name="Cichol's Mantle", augments={'STR+20','Accuracy+20 Attack+20','STR+10','Weapon skill damage +10%',}},
        waist="Fotia belt",
        legs="Sakpata's cuisses",
        feet="Nyame sollerets"
	}	
	
    sets.WS["Judgment"] = {
        ammo="knobkierrie",
        head="Agoge mask +3",
        neck="Warrior's bead necklace +2",
        ear1="Thrud earring",
        ear2="Moonshade earring",
        body="Nyame Mail",
        hands="Boii mufflers +3",
        ring1="Cornelia's ring",
        ring2="Sroda Ring",
        back={ name="Cichol's Mantle", augments={'STR+20','Accuracy+20 Attack+20','STR+10','Weapon skill damage +10%',}},
        waist="Sailfi belt +1",
        legs="Boii cuisses +3",
        feet="Nyame sollerets"
	}
	
    sets.WS.Cataclysm = {
        ammo="Knobkierrie",
        head="Pixie Hairpin +1",
        body="Nyame Mail",
        hands="Nyame Gauntlets",
        legs="Nyame Flanchard",
        feet="Nyame Sollerets",
        neck={ name="War. Beads +2", augments={'Path: A',}},
        waist={ name="Sailfi Belt +1", augments={'Path: A',}},
        left_ear="Friomisi Earring",
        right_ear={ name="Moonshade Earring", augments={'Accuracy+4','TP Bonus +250',}},
        left_ring="Archon Ring",
        right_ring="Cornelia's Ring",
        back={ name="Cichol's Mantle", augments={'STR+20','Accuracy+20 Attack+20','STR+10','Weapon skill damage +10%',}},
    }
	
	
	sets.WS["Fell Cleave"] = {
        ammo="knobkierrie",
        head="Agoge mask +3",
        neck="Fotia gorget",
        ear1="Thrud earring",
        ear2="Moonshade earring",
        body="Nyame Mail",
        hands="Boii Mufflers +3",
        ring1="Cornelia's ring",
        ring2="Sroda Ring",
        back={ name="Cichol's Mantle", augments={'STR+20','Accuracy+20 Attack+20','STR+10','Weapon skill damage +10%',}},
        waist="Fotia belt",
        legs="Boii cuisses +3",
        feet="Nyame sollerets"
    }

    sets.WS["Ukko's Fury"] = {
        ammo="Yetshila +1",
        head="Boii mask +3",
        neck="Warrior's bead necklace +2",
        ear1="Schere Earring",
        ear2="Boii earring +1",
        body="Hjarrandi breastplate",
        hands="Boii Mufflers +3",
        ring1="Begrudging Ring",
        ring2="Niqmaddu Ring",
        back={ name="Cichol's Mantle", augments={'STR+20','Accuracy+20 Attack+20','STR+10','Crit.hit rate+10','Damage taken-5%',}},
        waist={ name="Sailfi Belt +1", augments={'Path: A',}},
        legs="Boii cuisses +3",
        feet="Boii calligae +3"
	}
	
    sets.WS["Steel Cyclone"] = {
        ammo="knobkierrie",
        head="Agoge mask +3",
        neck="Warrior's bead necklace +2",
        ear1="Thrud earring",
        ear2="Moonshade earring",
        body="Nyame Mail",
        hands="Boii mufflers +3",
        ring1="Cornelia's Ring",
        ring2="Sroda Ring",
        back={ name="Cichol's Mantle", augments={'STR+20','Accuracy+20 Attack+20','STR+10','Weapon skill damage +10%',}},
        waist="Sailfi belt +1",
        legs="Boii cuisses +3",
        feet="Nyame sollerets"
    }
	
	sets.WS["Upheaval"] = {
        ammo="knobkierrie",
        head="Agoge mask +2",
        neck="Warrior's bead necklace +2",
        ear1="Moonshade Earring",
        ear2="Thrud earring",
        body="Sakpata's breastplate",
        hands="Boii Mufflers +3",
        ring1="Sroda Ring",
        ring2="Niqmaddu Ring",
        back={ name="Cichol's Mantle", augments={'STR+20','Accuracy+20 Attack+20','STR+10','Weapon skill damage +10%',}},
        waist={ name="Sailfi Belt +1", augments={'Path: A',}},
        legs="Boii cuisses +3",
        feet="Boii calligae +3"
	}
			
    sets.WS["Resolution"] = {
        ammo="knobkierrie",
        head="Agoge mask +3",
        neck="Fotia gorget",
        ear1="Thrud earring",
        ear2="Moonshade earring",
        body="Nyame Mail",
        hands={ name="Valorous Mitts", augments={'STR+10','Accuracy+23','Weapon skill damage +7%','Accuracy+3 Attack+3',}},
        ring1="Karieyh Ring",
        ring2="Niqmaddu Ring",
        back={ name="Cichol's Mantle", augments={'STR+20','Accuracy+20 Attack+20','STR+10','Weapon skill damage +10%',}},
        waist="Fotia belt",
        legs="Boii cuisses +2",
        feet="Nyame sollerets"
    }

    sets.WS["Impulse Drive"] = {
        ammo="Yetshila +1",
        head="Boii mask +3",
        neck="Warrior's bead necklace +2",
        ear1="Moonshade Earring",
        ear2="Boii earring +1",
        body="Hjarrandi breastplate",
        hands="Boii Mufflers +3",
        ring1="Begrudging Ring",
        ring2="Niqmaddu Ring",
        back={ name="Cichol's Mantle", augments={'STR+20','Accuracy+20 Attack+20','STR+10','Crit.hit rate+10','Damage taken-5%',}},
        waist={ name="Sailfi Belt +1", augments={'Path: A',}},
        legs="Boii cuisses +3",
        feet="Boii calligae +3"
    }

    sets.WS["Sonic Thrust"] = {
        ammo="knobkierrie",
        head="Agoge mask +3",
        neck="Warrior's bead necklace +2",
        ear1="Thrud earring",
        ear2="Moonshade earring",
        body="Pummeler's lorica +3",
        hands="Boii mufflers +3",
        ring1="Cornelia's Ring",
        ring2="Sroda Ring",
        back={ name="Cichol's Mantle", augments={'STR+20','Accuracy+20 Attack+20','STR+10','Weapon skill damage +10%',}},
        waist="Sailfi belt +1",
        legs="Boii cuisses +3",
        feet="Nyame sollerets"
    }
end

function precast(spell)
    if sets.JA[spell.english] then
        equip(sets.JA[spell.english])
    elseif sets.WS[spell.english] then
        if buffactive['terror'] or buffactive['petrified'] or buffactive['sleep'] or buffactive['stun'] or buffactive['amnesia'] or spell.target.distance > 9 then
            cancel_spell()
        else
            equip(sets.WS[spell.english])
        end
    end
end

function aftercast(spell)
    if player.status == 'Engaged' then
        equip(sets.TP[TPMode])
    else
        equip(sets.Idle)
    end
end

function status_change(new, old)
    if new == 'Engaged' then
        equip(sets.TP[TPMode])
    else
        equip(sets.Idle)
    end
end

function self_command(command)
    if command == 'TP' then
        if TPMode =="TP" then
            TPMode ="TwoHanded"
        elseif TPMode =="TwoHanded" then
            TPMode ="Hybrid"
		elseif TPMode =="Hybrid" then
			TPMode ="TP"
        end
        windower.add_to_chat('TP mode is now: '..TPMode)
        equip(sets.TP[TPMode])
    elseif command == 'Weapon' then
        if Weapon == "Sword" then
            Weapon = "Club"
            equip({
                main="Loxotic Mace +1",
                sub="Blurred Shield +1",
            })
        elseif Weapon == "Club" then
            Weapon = "Great Axe"
            equip({
                main="Ukonvasara",
                sub="Utu Grip",
            })
        elseif Weapon == "Great Axe" then
            Weapon = "Polearm"
            equip({
                main="Shining One",
                sub="Utu Grip",
            })
        elseif Weapon == "Polearm" then
            Weapon = "Sword"
            equip({
                main="Naegling",
                sub="Blurred Shield +1",
            }) 
        end
    end
    windower.add_to_chat('Weapon is now: '..Weapon)
end
if command == 'ws' then
    if Weapon == 'Sword' then
        send_command("input /ws 'Savage Blade' <t>")
    elseif Weapon == "Club" then
        send_command("input /ws 'Judgement' <t>")
    elseif Weapon == "Great Axe" then
        send_command("input /ws 'Ukko's Fury' <t>")
    elseif Weapon == "Polearm" then
        send_command("input /ws 'Impulse Drive' <t>")
    end
end
