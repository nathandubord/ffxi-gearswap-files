include('organizer-lib')

-- Gear Sets
function get_sets()
    sets.WS = {}

    -- Modes and their default values
		
    sets.WS['Cataclysm'] = {
    main="Xoanon",
    sub="Bloodrain Strap",
    ammo="Knobkierrie",
    head="Pixie Hairpin +1",
    body="Nyame Mail",
    hands={ name="Herculean Gloves", augments={'"Mag.Atk.Bns."+24','Haste+1','"Refresh"+2','Accuracy+2 Attack+2','Mag. Acc.+18 "Mag.Atk.Bns."+18',}},
    legs={ name="Herculean Trousers", augments={'STR+9','"Mag.Atk.Bns."+29','Weapon skill damage +1%','Mag. Acc.+18 "Mag.Atk.Bns."+18',}},
    feet={ name="Adhe. Gamashes +1", augments={'DEX+12','AGI+12','Accuracy+20',}},
    neck="Fotia Gorget",
    waist="Fotia Belt",
    left_ear="Friomisi Earring",
    right_ear="Hecate's Earring",
    left_ring="Archon Ring",
    right_ring="Cornelia's Ring",
    back={ name="Segomo's Mantle", augments={'DEX+20','Accuracy+20 Attack+20','DEX+10','"Dbl.Atk."+10','Damage taken-5%',}},
}
	
	sets.WS['Shijin Spiral'] = {
    ammo="Knobkierrie",
    head="Malignance Chapeau",
    body="Malignance Tabard",
    hands="Malignance Gloves",
    legs="Mpaca's Hose",
    feet="Mpaca's Boots",
    neck="Fotia Gorget",
    waist="Moonbow Belt +1",
    left_ear="Sherida Earring",
    right_ear={ name="Moonshade Earring", augments={'Accuracy+4','TP Bonus +250',}},
    left_ring="Gere Ring",
    right_ring="Niqmaddu Ring",
    back={ name="Segomo's Mantle", augments={'DEX+20','Accuracy+20 Attack+20','DEX+10','"Dbl.Atk."+10','Damage taken-5%',}},
}

	sets.WS['Asuran Fists'] = {
    main="Godhands",
    ammo="Knobkierrie",
    head="Mpaca's Cap",
    body="Bhikku Cyclas +3",
    hands="Mpaca's Gloves",
    legs="Mpaca's Hose",
    feet="Mpaca's Boots",
    neck={ name="Mnk. Nodowa +2", augments={'Path: A',}},
    waist="Fotia Belt",
    left_ear="Sherida Earring",
    right_ear="Schere earring",
    left_ring="Cornelia's Ring",
    right_ring="Sroda ring",
    back={ name="Segomo's Mantle", augments={'DEX+20','Accuracy+20 Attack+20','DEX+10','"Dbl.Atk."+10','Damage taken-5%',}},
}

	sets.WS['Victory Smite'] = {
    main="Godhands",
    ammo="Knobkierrie",
    head="Mpaca's Cap",
    body="Bhikku Cyclas +3",
    hands="Mpaca's Gloves",
    legs="Mpaca's Hose",
    feet="Mpaca's Boots",
    neck={ name="Mnk. Nodowa +2", augments={'Path: A',}},
    waist="Fotia Belt",
    left_ear="Sherida Earring",
    right_ear="Schere earring",
    left_ring="Gere Ring",
    right_ring="Niqmaddu Ring",
    back={ name="Segomo's Mantle", augments={'DEX+20','Accuracy+20 Attack+20','DEX+10','"Dbl.Atk."+10','Damage taken-5%',}},
}
	sets.WS['Dragon Kick'] = {
    main="Godhands",
    ammo="Knobkierrie",
    head="Mpaca's Cap",
    body="Bhikku Cyclas +3",
    hands="Mpaca's Gloves",
    legs="Mpaca's Hose",
    feet="Anch. Gaiters +2",
    neck={ name="Mnk. Nodowa +2", augments={'Path: A',}},
    waist="Fotia Belt",
    left_ear="Moonshade Earring",
    right_ear="Schere earring",
    left_ring="Cornelia's Ring",
    right_ring="Sroda ring",
    back={ name="Segomo's Mantle", augments={'DEX+20','Accuracy+20 Attack+20','DEX+10','"Dbl.Atk."+10','Damage taken-5%',}},
}
	sets.WS['Raging Fists'] = {
    main="Godhands",
    ammo="Knobkierrie",
    head="Mpaca's Cap",
    body="Bhikku Cyclas +3",
    hands="Mpaca's Gloves",
    legs="Mpaca's Hose",
    feet="Anch. Gaiters +2",
    neck={ name="Mnk. Nodowa +2", augments={'Path: A',}},
    waist="Fotia Belt",
    left_ear="Moonshade Earring",
    right_ear="Schere earring",
    left_ring="Cornelia's Ring",
    right_ring="Sroda ring",
    back={ name="Segomo's Mantle", augments={'DEX+20','Accuracy+20 Attack+20','DEX+10','"Dbl.Atk."+10','Damage taken-5%',}},
}
	sets.WS['Tornado Kick'] = {
    main="Godhands",
    ammo="Knobkierrie",
    head="Mpaca's Cap",
    body="Bhikku Cyclas +3",
    hands="Mpaca's Gloves",
    legs="Mpaca's Hose",
    feet="Anch. Gaiters +2",
    neck={ name="Mnk. Nodowa +2", augments={'Path: A',}},
    waist="Fotia Belt",
    left_ear="Moonshade Earring",
    right_ear="Schere earring",
    left_ring="Gere Ring",
    right_ring="Niqmaddu Ring",
    back={ name="Segomo's Mantle", augments={'DEX+20','Accuracy+20 Attack+20','DEX+10','"Dbl.Atk."+10','Damage taken-5%',}},
}
	sets.WS['Howling Fist'] = {
    main="Godhands",
    ammo="Knobkierrie",
    head="Mpaca's Cap",
    body="Bhikku Cyclas +3",
    hands="Mpaca's Gloves",
    legs="Mpaca's Hose",
    feet="Anch. Gaiters +2",
    neck={ name="Mnk. Nodowa +2", augments={'Path: A',}},
    waist="Fotia Belt",
    left_ear="Moonshade Earring",
    right_ear="Schere earring",
    left_ring="Gere Ring",
    right_ring="Niqmaddu Ring",
    back={ name="Segomo's Mantle", augments={'DEX+20','Accuracy+20 Attack+20','DEX+10','"Dbl.Atk."+10','Damage taken-5%',}},
}
	sets.WS["Ascetic's Fury"] = {
    main="Godhands",
    ammo="Knobkierrie",
    head="Mpaca's Cap",
    body="Bhikku Cyclas +3",
    hands="Mpaca's Gloves",
    legs="Mpaca's Hose",
    feet="Mpaca's Boots",
    neck={ name="Mnk. Nodowa +2", augments={'Path: A',}},
    waist="Moonbow Belt +1",
    left_ear="Sherida Earring",
    right_ear="Schere earring",
    left_ring="Gere Ring",
    right_ring="Niqmaddu ring",
    back={ name="Segomo's Mantle", augments={'DEX+20','Accuracy+20 Attack+20','DEX+10','"Dbl.Atk."+10','Damage taken-5%',}},
}



    sets.TP = {
    main="Godhands",
    ammo="Coiste Bodhar",
    head="Adhemar Bonnet +1",
    body="Bhikku Cyclas +3",
    hands={ name="Adhemar Wrist. +1", augments={'DEX+12','AGI+12','Accuracy+20',}},
    legs="Bhikku Hose +3",
    feet="Anch. Gaiters +3",
    neck={ name="Mnk. Nodowa +2", augments={'Path: A',}},
    waist="Moonbow Belt +1",
    left_ear="Sherida Earring",
    right_ear="Mache earring +1",
    left_ring="Gere Ring",
    right_ring="Niqmaddu Ring",
    back={ name="Segomo's Mantle", augments={'DEX+20','Accuracy+20 Attack+20','DEX+10','"Dbl.Atk."+10','Damage taken-5%',}},
}

    sets.Idle = {
        main="",
        sub="",
    ammo="Staunch Tathlum",
    head="Malignance Chapeau",
    body="Mpaca's Doublet",
    hands="Nyame Gauntlets",
    legs="Bhikku Hose +3",
    feet="Malignance Boots",
    neck={ name="Loricate Torque +1", augments={'Path: A',}},
    waist="Moonbow Belt +1",
    left_ear="Odnowa Earring +1",
    right_ear={ name="Bhikku Earring +2", augments={'System: 1 ID: 1676 Val: 0','Accuracy+16','Mag. Acc.+16','"Store TP"+6','STR+7 DEX+7',}},
    left_ring="Defending Ring",
    right_ring="Shneddick Ring",
    back={ name="Segomo's Mantle", augments={'DEX+20','Accuracy+20 Attack+20','DEX+10','"Dbl.Atk."+10','Damage taken-5%',}},
}



end

function precast(spell)
	if spell.type == "WeaponSkill" then
        equip(sets.WS[spell.english])
    end
end

function aftercast(spell)
    if player.status == 'Engaged' then
        equip(sets.TP)
    else
        equip(sets.Idle)
    end
end

function status_change(new,old)
    if T{'Idle','Resting'}:contains(new) then
        equip(sets.Idle)
    elseif new == 'Engaged' then
        equip(sets.TP)
    end
end

Zone_change_delay = 5
windower.raw_register_event('zone change', function (new_id)
    local zones = gearswap.res.zones
    local new_zone = zones[new_id].name
    coroutine.schedule(gearswap.equip_sets:prepare('zone_change', nil, new_zone),(Zone_change_delay))
end)

function zone_change(zoneName)
    if zoneName:endswith('Adoulin') then
        equip({ body="Councilor's Garb" })
    end
end
