include('organizer-lib')

-- Gear Sets
function get_sets()
    sets.WS = {}
    TPMode = 'Standard'

    -- Modes and their default values
    sets.WS['Victory Smite'] = {
        ammo="Honed Tathlum",
        head={ name="Blistering Sallet +1", augments={'Path: A',}},
        body="Mummu Jacket +2",
        hands={ name="Adhemar Wrist. +1", augments={'STR+12','DEX+12','Attack+20',}},
        legs="Mummu Kecks +2",
        feet="Mummu Gamash. +2",
        neck="Fotia Gorget",
        waist="Fotia Belt",
        left_ear="Odr earring",
        right_ear={ name="Moonshade Earring", augments={'Accuracy+4','TP Bonus +250',}},
        left_ring="Niqmaddu Ring",
        right_ring="Gere Ring",
        back={ name="Segomo's Mantle", augments={'DEX+20','Accuracy+20 Attack+20','DEX+10','"Dbl.Atk."+10','Phys. dmg. taken-10%',}},
    }

    sets.WS['Shijin Spiral'] = {
        ammo="Honed Tathlum",
        head={ name="Adhemar Bonnet +1", augments={'STR+12','DEX+12','Attack+20',}},
        neck="Fotia Gorget",
        left_ear="Mache Earring +1",
        right_ear="Moonshade Earring",
        body="Mummu Jacket +2",
        hands={ name="Adhemar Wrist. +1", augments={'STR+12','DEX+12','Attack+20',}},
        left_ring="Epona's Ring",
        right_ring="Begrudging Ring",
        back={ name="Segomo's Mantle", augments={'DEX+20','Accuracy+20 Attack+20','DEX+10','"Dbl.Atk."+10','Phys. dmg. taken-10%',}},
        waist="Fotia Belt",
        legs="Hiza. Hizayoroi +1",
        feet={ name="Herculean Boots", augments={'Attack+28','"Triple Atk."+4','STR+1',}},
    }

    sets.WS['Tornado Kick'] = {
        ammo={ name="Coiste Bodhar", augments={'Path: A',}},
        head={ name="Nyame Helm", augments={'Path: B',}},
        body={ name="Nyame Mail", augments={'Path: B',}},
        hands={ name="Nyame Gauntlets", augments={'Path: B',}},
        legs={ name="Nyame Flanchard", augments={'Path: B',}},
        feet={ name="Nyame Sollerets", augments={'Path: B',}},
        neck="Fotia Gorget",
        waist="Moonbow Belt",
        right_ear={ name="Moonshade Earring", augments={'Accuracy+4','TP Bonus +250',}},
        left_ear="Sherida Earring",
        left_ring="Gere Ring",
        right_ring="Niqmaddu Ring",
        back={ name="Segomo's Mantle", augments={'DEX+20','Accuracy+20 Attack+20','DEX+10','"Dbl.Atk."+10','Phys. dmg. taken-10%',}},
    }

    sets.TP = {}

    sets.TP.Standard = {
        main="Godhands",
        ammo="Honed Tathlum",
        head={ name="Adhemar Bonnet +1", augments={'STR+12','DEX+12','Attack+20',}},
        body={ name="Adhemar Jacket +1", augments={'DEX+12','AGI+12','Accuracy+20',}},
        hands={ name="Adhemar Wrist. +1", augments={'STR+12','DEX+12','Attack+20',}},
        legs="Mummu kecks +2",
        feet="Mummu Gamash. +2",
        neck="Moonlight Nodowa",
        waist="Moonbow Belt",
        left_ear="Sherida Earring",
        right_ear="Mache Earring +1",
        left_ring="Petrov Ring",
        right_ring="Epona's Ring",
        back={ name="Segomo's Mantle", augments={'DEX+20','Accuracy+20 Attack+20','DEX+10','"Dbl.Atk."+10','Phys. dmg. taken-10%',}},
    }

    sets.TP.Hybrid = set_combine(sets.TP.Standard, {
        main={ name="Godhands", augments={'Path: A',}},
        ammo="Staunch Tathlum +1",
        head="Malignance Chapeau",
        body="Malignance Tabard",
        hands="Malignance Gloves",
        legs="Malignance Tights",
        feet="Malignance Boots",
        neck="Moonlight Nodowa",
        waist="Moonbow Belt",
        left_ear="Sherida Earring",
        right_ear="Telos Earring",
        left_ring="Gere Ring",
        right_ring="Niqmaddu Ring",
        back={ name="Segomo's Mantle", augments={'DEX+20','Accuracy+20 Attack+20','DEX+10','"Dbl.Atk."+10','Phys. dmg. taken-10%',}},
    })
end

function precast(spell)
	if spell.type == "WeaponSkill" then
        equip(sets.WS[spell.english])
    end
end

function aftercast(spell)
    if player.status == 'Engaged' then
        equip(sets.TP[TPMode])
    end
end

function status_change(new,old)
    if new == 'Engaged' then
        equip(sets.TP[TPMode])
    end
end

function self_command(command)
    if command == 'TP' then
        if TPMode =="Standard" then
            TPMode ="Hybrid"
        elseif TPMode =="Hybrid" then
            TPMode ="Standard"
        end
        windower.add_to_chat('TP mode is now: '..TPMode)
        equip(sets.TP[TPMode])
    end
end
