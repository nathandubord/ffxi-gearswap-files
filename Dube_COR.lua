function get_sets()
    -- Basic sets
    sets.Idle = {
        main="Naegling",
        sub="Gleti's Knife",
        head="Malignance Chapeau",
        body="Malignance Tabard",
        hands="Malignance Gloves",
        legs="Malignance Tights",
        feet="Malignance Boots",
        neck={ name="Loricate Torque +1", augments={'Path: A',}},
        waist="Plat. Mog. Belt",
        left_ear="Telos Earring",
        right_ear="Brutal Earring",
        left_ring="Defending Ring",
        right_ring="Shneddick Ring",
        back="Shadow Mantle",
    }

    TPMode = 'TP'
        sets.TP = {}
        sets.TP.TP = {
        head="Malignance Chapeau",
        body="Malignance Tabard",
        hands="Malignance Gloves",
        legs="Malignance Tights",
        feet="Malignance Boots",
        neck="Combatant's Torque",
        waist={ name="Sailfi Belt +1", augments={'Path: A',}},
        left_ear="Telos Earring",
        right_ear="Brutal Earring",
        left_ring="Chirich Ring +1",
        right_ring="Ilabrat Ring",
        back={ name="Camulus's Mantle", augments={'DEX+20','Accuracy+20 Attack+20','Accuracy+5','"Dbl.Atk."+10','Damage taken-5%',}},
    }

	sets.TP.Ranged = {
    main="Naegling",
    sub="Nusku Shield",
    range="Death Penalty",
    ammo="Orichalcum Bullet",
    head="Chass. Tricorne +2",
    body="Laksa. Frac +3",
    hands={ name="Lanun Gants +3", augments={'Enhances "Fold" effect',}},
    legs={ name="Adhemar Kecks +1", augments={'AGI+12','"Rapid Shot"+13','Enmity-6',}},
    feet="Malignance Boots",
    neck={ name="Comm. Charm +2", augments={'Path: A',}},
    waist="Yemaya Belt",
    left_ear="Telos Earring",
    right_ear="Neritic Earring",
    left_ring="Dingir Ring",
    right_ring="Ilabrat Ring",
    back={ name="Camulus's Mantle", augments={'AGI+20','Rng.Acc.+20 Rng.Atk.+20','"Snapshot"+10',}},
}

    -- Job ability sets
    sets.precast = {}
    sets.precast.JA = {}
	sets.precast.JA.CorsairRoll = {
        main="Rostam",
        range="Compensator",
        head="Lanun Tricorne +1",
        hands="Chasseur's Gants +3",
        neck="Regal Necklace",
        ring1="Barataria Ring",
        ring2="Luzaf's Ring",
    }
    -- Weaponskill sets
    sets.WS = {}
    sets.WS["Savage Blade"] = {
    main="Naegling",
    sub="Gleti's Knife",
    ammo="Oshasha's Treatise",
    head={ name="Herculean Helm", augments={'Attack+19','Pet: STR+1','Weapon skill damage +6%','Accuracy+14 Attack+14','Mag. Acc.+17 "Mag.Atk.Bns."+17',}},
    body="Laksa. Frac +3",
    hands="Chasseur's Gants +3",
    legs={ name="Herculean Trousers", augments={'"Mag.Atk.Bns."+25','Crit. hit damage +3%','Weapon skill damage +5%','Accuracy+7 Attack+7','Mag. Acc.+1 "Mag.Atk.Bns."+1',}},
    feet={ name="Lanun Bottes +3", augments={'Enhances "Wild Card" effect',}},
    neck="Rep. Plat. Medal",
    waist={ name="Sailfi Belt +1", augments={'Path: A',}},
    left_ear="Ishvara Earring",
    right_ear={ name="Moonshade Earring", augments={'Accuracy+4','TP Bonus +250',}},
    left_ring="Cornelia's Ring",
    right_ring="Epaminondas's Ring",
    back={ name="Camulus's Mantle", augments={'STR+20','Accuracy+20 Attack+20','STR+10','Weapon skill damage +10%','Damage taken-5%',}},
}
	
	sets.WS["Wildfire"] = {
    main="Naegling",
    sub="Nusku Shield",
    range="Death Penalty",
    ammo="Living Bullet",
    head={ name="Herculean Helm", augments={'"Avatar perpetuation cost" -2','"Mag.Atk.Bns."+19','Accuracy+13 Attack+13','Mag. Acc.+18 "Mag.Atk.Bns."+18',}},
    body={ name="Lanun Frac +3", augments={'Enhances "Loaded Deck" effect',}},
    hands={ name="Herculean Gloves", augments={'"Mag.Atk.Bns."+24','Haste+1','"Refresh"+2','Accuracy+2 Attack+2','Mag. Acc.+18 "Mag.Atk.Bns."+18',}},
    legs={ name="Herculean Trousers", augments={'STR+9','"Mag.Atk.Bns."+29','Weapon skill damage +1%','Mag. Acc.+18 "Mag.Atk.Bns."+18',}},
    feet={ name="Lanun Bottes +3", augments={'Enhances "Wild Card" effect',}},
    neck={ name="Comm. Charm +2", augments={'Path: A',}},
    waist="Fotia Belt",
    left_ear="Friomisi Earring",
    right_ear={ name="Moonshade Earring", augments={'Accuracy+4','TP Bonus +250',}},
    left_ring="Cornelia's Ring",
    right_ring="Dinger Ring",
    back={ name="Camulus's Mantle", augments={'AGI+20','Mag. Acc+20 /Mag. Dmg.+20','AGI+10','Weapon skill damage +10%',}},
}
	
	sets.WS["Leaden Salute"] = {
    main="Naegling",
    sub="Gleti's Knife",
    range="Death Penalty",
    ammo="Orichalcum Bullet",
    head="Pixie Hairpin +1",
    body={ name="Lanun Frac +3", augments={'Enhances "Loaded Deck" effect',}},
    hands={ name="Herculean Gloves", augments={'"Mag.Atk.Bns."+24','Haste+1','"Refresh"+2','Accuracy+2 Attack+2','Mag. Acc.+18 "Mag.Atk.Bns."+18',}},
    legs={ name="Herculean Trousers", augments={'STR+9','"Mag.Atk.Bns."+29','Weapon skill damage +1%','Mag. Acc.+18 "Mag.Atk.Bns."+18',}},
    feet={ name="Lanun Bottes +3", augments={'Enhances "Wild Card" effect',}},
    neck={ name="Comm. Charm +2", augments={'Path: A',}},
    waist="Eschan Stone",
    left_ear="Friomisi Earring",
    right_ear={ name="Moonshade Earring", augments={'Accuracy+4','TP Bonus +250',}},
    left_ring="Archon Ring",
    right_ring="Cornelia's Ring",
    back={ name="Camulus's Mantle", augments={'AGI+20','Mag. Acc+20 /Mag. Dmg.+20','AGI+10','Weapon skill damage +10%',}},
}
	
	
end

ROLLS = S{
    "Corsair's Roll",
    "Ninja Roll",
    "Hunter's Roll",
    "Chaos Roll",
    "Magus's Roll",
    "Healer's Roll",
    "Puppet Roll",
    "Choral Roll",
    "Monk's Roll",
    "Beast Roll",
    "Samurai Roll",
    "Evoker's Roll",
    "Rogue's Roll",
    "Warlock's Roll",
    "Fighter's Roll",
    "Drachen Roll",
    "Gallant's Roll",
    "Wizard's Roll",
    "Dancer's Roll",
    "Scholar's Roll",
    "Bolter's Roll",
    "Caster's Roll",
    "Courser's Roll",
    "Blitzer's Roll",
    "Tactician's Roll",
    "Allies' Roll",
    "Miser's Roll",
    "Companion's Roll",
    "Avenger's Roll",
    "Runeist's Roll",
    "Naturalist's Roll",
}

function precast(spell)
    if ROLLS:contains(spell.english) then
        equip(sets.precast.JA.CorsairRoll)
    elseif sets.WS[spell.english] then
        equip(sets.WS[spell.english])
    end
end

-- Whenever you use a JA or a WS it will look for the set with that name
function midcast(spell, act)

end

function aftercast(spell)
    if player.status == 'Engaged' then
        equip(sets.TP[TPMode])
    else
        equip(sets.Idle)
    end
end

function status_change(new, old)
    if new == 'Engaged' then
        equip(sets.TP[TPMode])
    else
        equip(sets.Idle)
    end
end

function self_command(command)
    if command == 'TP' then
        if TPMode =="TP" then
            TPMode ="Ranged"
        elseif TPMode =="Ranged" then
            TPMode ="TP"
        end
        windower.add_to_chat('TP mode is now: '..TPMode)
        equip(sets.TP[TPMode])
    end
end

Zone_change_delay = 5
windower.raw_register_event('zone change', function (new_id)
    local zones = gearswap.res.zones
    local new_zone = zones[new_id].name
    coroutine.schedule(gearswap.equip_sets:prepare('zone_change', nil, new_zone),(Zone_change_delay))
end)

function zone_change(zoneName)
    if zoneName:endswith('Adoulin') then
        equip({ body="Councilor's Garb" })
    end
end
