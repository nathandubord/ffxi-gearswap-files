function get_sets()
    sets.Idle = {
        left_ring="Stikini Ring +1",
        main="Malignance Pole",
        sub="Enki Strap",
        ammo="Homiliary",
        head={ name="Nyame Helm", augments={'Path: B',}},
        body="Shamash Robe",
        hands={ name="Nyame Gauntlets", augments={'Path: B',}},
        legs={ name="Nyame Flanchard", augments={'Path: B',}},
        feet={ name="Nyame Sollerets", augments={'Path: B',}},
        neck={ name="Loricate Torque +1", augments={'Path: A',}},
        waist="Carrier's Sash",
        left_ear="Eabani Earring",
        right_ear={ name="Ebers Earring +1", augments={'System: 1 ID: 1676 Val: 0','Accuracy+14','Mag. Acc.+14','Damage taken-5%',}},
        right_ring="Stikini Ring +1",
        back={ name="Alaunus's Cape", augments={'MND+20','Eva.+20 /Mag. Eva.+20','Mag. Evasion+10','"Cure" potency +10%','Mag. Evasion+12',}},
    }

    sets.TP = {
        main="Maxentius",
        sub="Ammurapi Shield",
        ammo="Amar Cluster",
        head="Aya. Zucchetto +2",
        body="Ayanmo Corazza +2",
        hands="Bunzi's Gloves",
        legs={ name="Nyame Flanchard", augments={'Path: B',}},
        feet="Aya. Gambieras +2",
        neck="Sanctity Necklace",
        waist="Plat. Mog. Belt",
        left_ear="Dedition Earring",
        right_ear={ name="Ebers Earring +1", augments={'System: 1 ID: 1676 Val: 0','Accuracy+14','Mag. Acc.+14','Damage taken-5%',}},
        left_ring="Chirich Ring +1",
        right_ring="Defending Ring",
        back={ name="Alaunus's Cape", augments={'MND+20','Eva.+20 /Mag. Eva.+20','Mag. Evasion+10','"Cure" potency +10%','Mag. Evasion+12',}},
    }

    sets.WS = {}
    sets.WS['Black Halo'] = {
        main="Maxentius",
        sub="Ammurapi Shield",
        ammo="Oshasha's Treatise",
        head={ name="Nyame Helm", augments={'Path: B',}},
        body={ name="Nyame Mail", augments={'Path: B',}},
        hands={ name="Nyame Gauntlets", augments={'Path: B',}},
        legs={ name="Nyame Flanchard", augments={'Path: B',}},
        feet={ name="Nyame Sollerets", augments={'Path: B',}},
        neck="Sanctity Necklace",
        waist="Fotia Belt",
        left_ear={ name="Moonshade Earring", augments={'Accuracy+4','TP Bonus +250',}},
        right_ear="Brutal Earring",
        left_ring={ name="Metamor. Ring +1", augments={'Path: A',}},
        right_ring="Cornelia's Ring",
        back={ name="Alaunus's Cape", augments={'MND+20','Eva.+20 /Mag. Eva.+20','Mag. Evasion+10','"Cure" potency +10%','Mag. Evasion+12',}},   
    }

    sets.precast = {}

    -- 20% from /rdm FC 3 trait
    -- 62% from gear
    sets.precast.FC = {
        ammo="Sapience Orb",
        head="Ebers Cap +2",
        body="Inyanga Jubbah +2",
        hands={ name="Fanatic Gloves", augments={'MP+5','Healing magic skill +2','"Fast Cast"+1',}},
        legs="Pinga pants +1",
        feet="Chelona Boots",
        neck="Orunmila's Torque",
        waist="Embla Sash",
        left_ear="Malignance Earring",
        right_ear="Loquac. Earring",
        left_ring="Kishar Ring",
        right_ring="Weather. Ring",
        back="Swith Cape +1"
    }

    sets.midcast = {};
    sets.midcast.Cure = {
        main="Queller Rod",
        sub="Ammurapi Shield",
        ammo="Staunch tathlum +1",
        head="Ebers Cap +2",
        body="Ebers Bliaut +2",
        hands="Kaykaus Cuffs +1",
        legs="Ebers Pant. +2",
        feet="Kaykaus Boots +1",
        neck="Cleric's Torque",
        waist="Plat. Mog. Belt",
        left_ear="Magnetic Earring",
        right_ear="Glorious Earring",
        left_ring="Ilabrat ring",
        right_ring="Defending Ring",
        back="Moonlight cape"
    }

    sets.midcast.Curaga = {
        main="Queller Rod",
        sub="Ammurapi Shield",
        head="Ebers Cap +2",
        body="Ebers Bliaut +2",
        hands="Kaykaus Cuffs +1",
        legs="Ebers Pant. +2",
        feet="Kaykaus boots +1",
        neck="Cleric's Torque",
        waist="Plat. Mog. Belt",
        left_ear="Magnetic Earring",
        right_ear="Glorious Earring",
        left_ring="Stikini Ring +1",
        right_ring="Naji's Loop",
        back={ name="Alaunus's Cape", augments={'MND+20','Eva.+20 /Mag. Eva.+20','Mag. Evasion+10','"Cure" potency +10%','Mag. Evasion+12',}},
    }

    sets.midcast.Cursna = {
        main="Beneficus",
        sub="Ammurapi Shield",
        head="Ebers Cap +2",
        body="Ebers Bliaut +2",
        hands={ name="Fanatic Gloves", augments={'MP+5','Healing magic skill +2','"Fast Cast"+1',}},
        feet="Orsn. Duckbills +2",
        left_ear="Magnetic Earring",
        right_ear={ name="Ebers Earring +1", augments={'System: 1 ID: 1676 Val: 0','Accuracy+14','Mag. Acc.+14','Damage taken-5%',}},
        left_ring="Ephedra Ring",
        right_ring="Stikini Ring +1",
        back={ name="Alaunus's Cape", augments={'MND+20','Eva.+20 /Mag. Eva.+20','Mag. Evasion+10','"Cure" potency +10%','Mag. Evasion+12',}},
    }

    sets.midcast.Enfeebling = {
        main="Bunzi's Rod",
        sub="Ammurapi Shield",
        ammo="Hydrocera",
        head="Ebers Cap +2",
        body="Ebers Bliaut +2",
        hands="Regal Cuffs",
        legs={ name="Chironic Hose", augments={'Mag. Acc.+29','Haste+1','MND+11',}},
        feet="Theo. Duckbills +1",
        neck="Erra pendant",
        waist="Rumination Sash",
        left_ear="Regal Earring",
        right_ear={ name="Ebers Earring +1", augments={'System: 1 ID: 1676 Val: 0','Accuracy+14','Mag. Acc.+14','Damage taken-5%',}},
        left_ring="Stikini Ring +1",
        right_ring="Stikini Ring +1",
        back="Twilight Cape",
    }

    sets.midcast.EnhancingSkill = {
        main="Beneficus",
        sub="Ammurapi Shield",
        head="Befouled Crown",
        body={ name="Telchine Chas.", augments={'Enh. Mag. eff. dur. +10',}},
        hands="Ayao's Gages",
        legs="Clr. Pantaln. +2",
        feet="Orsn. Duckbills +2",
        neck="Incanter's Torque",
        waist="Olympus Sash",
        left_ear="Magnetic Earring",
        left_ring="Stikini Ring +1",
        right_ring="Stikini Ring +1",
    }

    sets.midcast.Nuking = {
        main="Daybreak",
        sub="Culminus",
        head="Bunzi's Hat",
        neck="Sibyl Scarf",
        body="Bunzi's Robe",
        hands="Bunzi's Gloves",
        legs="Bunzi's Pants",
        feet="Bunzi's Sabots",
        waist="Eschan Stone",
        left_ear="Hecate's Earring",
        right_ear="Malignance Earring",
        left_ring="Stikini Ring",
        right_ring="Stikini Ring",
        back="Searing Cape",
    }

    sets.midcast.EnhancingDuration = set_combine(sets.midcast.EnhancingSkill, {
        main="Gada",
        sub="Ammurapi shield",
        head="Telchine Cap",
        body="Telchine Chas.",
        hands="Telchine Gloves",
        legs="Telchine Braconi",
        feet="Telchine Pigaches",
        waist="Embla Sash",
    })

    sets.midcast.Recast = {
        head="Inyanga Tiara +2",
        body="Ayanmo Corazza +2",
        hands="Inyan. Dastanas +2",
        legs="Aya. Cosciales +2",
        feet="Nyame sollerets",
    }

    sets.midcast.Erase = set_combine(sets.midcast.Recast, {
        neck="Cleric's torque",
        head="Ebers cap +2",
    })

    sets.midcast.BarSpells = set_combine(sets.midcast.EnhancingSkill, {
        main="Beneficus",
        head="Ebers cap +2",
        body="Ebers Bliaut +2",
        feet="Orsn. Duckbills +2",
    })

    sets.midcast.Auspice = set_combine(sets.midcast.EnhancingDuration, {
        feet="Orsn. Duckbills +2",
    })
end

Barspells = S{'Barfira','Barfire','Barwater','Barwatera','Barstone','Barstonra','Baraero','Baraera','Barblizzara','Barblizzard','Barthunder', 'Barthundra', 'Barparalyze', 'Barparalyzra', 'Barsleep', 'Barsleepra', 'Barpoison', 'Barpoisonra', 'Barblind', 'Barblindra', 'Barsilence', 'Barsilencera', 'Barpetrify', 'Barpetra', 'Barvirus', 'Barvira'}
Boosts = S{'Boost-AGI', 'Boost-CHR', 'Boost-DEX', 'Boost-INT', 'Boost-MND', 'Boost-STR', 'Boost-VIT'}
Cures = S{'Cure','Cure II','Cure III','Cure IV','Cure V','Cure VI'}
Curagas = S{'Curaga','Curaga II','Curaga III','Curaga IV','Curaga V','Cura','Cura II','Cura III'}
Removals = S{'Paralyna','Silena','Viruna', 'Stona','Blindna','Poisona'}
Raises = S{'Raise', 'Raise II', 'Raise III', 'Arise', 'Reraise', 'Reraise II', 'Reraise III', 'Reraise IV'}

function precast(spell)
    if spell.action_type == 'Magic' then
        equip(sets.precast.FC)
    elseif spell.type == "WeaponSkill" then
        equip(sets.WS[spell.english])
    end
end

function midcast(spell)
    if spell.skill =='Healing Magic' then
        if Cures:contains(spell.name) then
            equip(sets.midcast.Cure)
        elseif Curagas:contains(spell.name) then
            equip(sets.midcast.Curaga)
        elseif Removals:contains(spell.name) then
            equip(sets.midcast.Erase)
        elseif Raises:contains(spell.name) then
            equip(sets.midcast.Recast)
        elseif spell.name == 'Cursna' then
            equip(sets.midcast.Cursna)
        end
    elseif spell.skill =='Enhancing Magic' then
        if Barspells:contains(spell.name) then
            equip(sets.midcast.BarSpells)
        elseif Boosts:contains(spell.name) then
            equip(sets.midcast.Boost)
        elseif spell.name == "Auspice" then
            equip(sets.midcast.Auspice)
        elseif spell.name == 'Erase' then
            equip(sets.midcast.Erase)
        else
            equip(sets.midcast.EnhancingDuration)
        end
    elseif spell.skill =='Enfeebling Magic' then
        equip(sets.midcast.Enfeebling)
    elseif spell.skill =='Divine Magic' then
        equip(sets.midcast.Nuking)
    end
end

function aftercast(spell)
    if player.status == 'Engaged' then
        equip(sets.TP)
    else
        equip(sets.Idle)
    end
end

function status_change(new, old)
    if new == 'Engaged' then
        equip(sets.TP)
    else
        equip(sets.Idle)
    end
end
