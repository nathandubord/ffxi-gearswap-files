function get_sets()
	sets.WS = {}
	sets.WS['Savage Blade'] = {
	    ammo="Oshasha's Treatise",
		head="Nyame Helm",
		body="Nyame Mail",
		hands="Nyame Gauntlets",
		legs="Nyame Flanchard",
		feet="Nyame Sollerets",
		neck="Fotia Gorget",
		waist="Fotia Belt",
		left_ear={ name="Moonshade Earring", augments={'"Mag.Atk.Bns."+4','TP Bonus +250',}},
		right_ear="Brutal Earring",
		left_ring="Karieyh Ring +1",
		right_ring="Cornelia's Ring",
	}
	
	sets.Idle = {
	    ammo="Staunch Tathlum",
		head="Nyame Helm",
		body="Nyame Mail",
		hands="Nyame Gauntlets",
		legs={ name="Carmine Cuisses +1", augments={'Accuracy+20','Attack+12','"Dual Wield"+6',}},
		feet="Nyame Sollerets",
		neck={ name="Loricate Torque +1", augments={'Path: A',}},
		waist="Flume Belt +1",
		left_ear="Etiolation Earring",
		left_ring="Defending Ring",
		right_ring="Shadow Ring",
		back="Shadow Mantle",
	}

	sets.TP = {
	    ammo={ name="Coiste Bodhar", augments={'Path: A',}},
		head={ name="Adhemar Bonnet +1", augments={'DEX+12','AGI+12','Accuracy+20',}},
		body="Malignance Tabard",
		hands={ name="Adhemar Wrist. +1", augments={'DEX+12','AGI+12','Accuracy+20',}},
		legs="Malignance Tights",
		feet="Malignance Boots",
		neck={ name="Loricate Torque +1", augments={'Path: A',}},
		waist="Windbuffet Belt +1",
		left_ear="Brutal Earring",
		right_ear="Suppanomimi",
		left_ring="Defending Ring",
		right_ring="Epona's Ring",
		back={ name="Rosmerta's Cape", augments={'DEX+20','Accuracy+20 Attack+20','DEX+10','"Store TP"+10',}}
	}
	
	sets.Nuking = {
	    main="Naegling",
		sub="Maxentius",
		ammo="Ghastly Tathlum +1",
		body={ name="Cohort Cloak +1", augments={'Path: A',}},
		hands="Hashi. Bazu. +1",
		legs="Jhakri Slops +2",
		feet="Jhakri Pigaches +2",
		neck="Sibyl Scarf",
		waist="Eschan Stone",
		left_ear="Friomisi Earring",
		right_ear="Hecate's Earring",
		left_ring="Jhakri Ring",
		right_ring="Metamor. Ring +1",
		back="Cornflower Cape",
	}
	sets.FC = {
	    head={ name="Carmine Mask", augments={'Accuracy+15','Mag. Acc.+10','"Fast Cast"+3',}},
		body="Hashishin Mintan +1",
		hands={ name="Leyline Gloves", augments={'Accuracy+14','Mag. Acc.+13','"Mag.Atk.Bns."+13','"Fast Cast"+2',}},
		legs="Aya. Cosciales +2",
		feet={ name="Carmine Greaves", augments={'HP+60','MP+60','Phys. dmg. taken -3',}},
		neck="Orunmila's Torque",
		left_ear="Loquac. Earring",
		right_ear="Etiolation Earring",
		left_ring="Kishar Ring",
		right_ring="Prolix Ring",
	}
	sets.BlueMagicSkill = {
	    hands="Rawhide Gloves",
		legs="Hashishin Tayt +1",
		right_ear={ name="Hashishin Earring", augments={'System: 1 ID: 1676 Val: 0','Accuracy+9','Mag. Acc.+9',}},
		right_ring="Stikini Ring",
		back="Cornflower Cape",
	}
		
	sets.Cure = set_combine(sets.BlueMagicSkill, {
	    head="Hashishin Kavuk +1",
		body="Hashishin Mintan +1",
		hands="Hashi. Bazu. +1",
		legs="Hashishin Tayt +1",
		feet="Hashi. Basmak +1",
	})
	
	sets.MagicAccuracy = {
		body={ name="Cohort Cloak +1", augments={'Path: A',}},
		hands="Jhakri Cuffs +2",
		legs="Jhakri Slops +2",
		feet="Jhakri Pigaches +2",
		neck="Sanctity Necklace",
		waist="Eschan Stone",
		left_ear="Digni. Earring",
		right_ear={ name="Hashishin Earring", augments={'System: 1 ID: 1676 Val: 0','Accuracy+9','Mag. Acc.+9',}},
		left_ring="Stikini Ring",
		right_ring="Metamor. Ring +1",
		back="Cornflower Cape"
	}
end

function precast(spell)
	if spell.action_type == "Magic" then
		equip(sets.FC)
	elseif spell.action_type == "Weaponskill" then
		equip(sets.WS[spell.english])
	end
end

HealingSpells = S{'Pollen', 'Magic Fruit', 'Healing Breeze'}
MagicAccuracySpells = S{'Sheep Song', 'Dream flower', 'Reaving wind', 'Blank gaze',  'Jettatura', 'Cruel Joke', 'Silent Storm', 'Sudden lunge', 'Paralyzing Triad'}
BlueMagicSkillSpells = S{'Occultation'}

function midcast(spell)
	if HealingSpells:contains(spell.name) then
		equip(sets.Cure)
	elseif HealingSpells:contains(spell.name) then
		equip(sets.MagicAccuracy)
	elseif BlueMagicSkillSpells:contains(spell.name) then
		equip(sets.BlueMagicSkill)
	else
		equip(sets.Nuking)
	end
end

function aftercast(spell)
	if player.status == 'Engaged' then
		equip(sets.TP)
	else
		equip(sets.Idle)
	end
end 

function status_change(new, old)
	if T{'Idle', 'Resting'}:contains(new) then
		equip(sets.Idle)
	elseif new == "Engaged" then
		equip(sets.TP)
	end
end