include('organizer-lib')

function get_sets()
    sets.Idle = {
        main="Mpaca's Staff",
        sub="Enki Strap",
        ammo="Homiliary",
        head="Arbatel Bonnet +3",
        body="Arbatel Gown +2",
        hands={ name="Nyame Gauntlets", augments={'Path: B',}},
        legs={ name="Nyame Flanchard", augments={'Path: B',}},
        feet={ name="Nyame Sollerets", augments={'Path: B',}},
        neck="Warder's Charm +1",
        waist="Embla Sash",
        left_ear="Eabani Earring",
        right_ear="Hearty Earring",
        left_ring="Stikini Ring +1",
        right_ring="Stikini Ring +1",
        back="Moonlight Cape",
    }

    sets.Regen = {
        main={ name="Musa", augments={'Path: C',}},
        head="Arbatel Bonnet +3",
        back={ name="Lugh's Cape", augments={'INT+20','Mag. Acc+20 /Mag. Dmg.+20','INT+10','"Mag.Atk.Bns."+10',}},
        body={ name="Telchine Chas.", augments={'Enh. Mag. eff. dur. +10',}},
        hands="Arbatel Bracers +3",
        legs={ name="Telchine Braconi", augments={'Enh. Mag. eff. dur. +10',}},
        feet={ name="Telchine Pigaches", augments={'Enh. Mag. eff. dur. +10',}},
        waist="Embla Sash",
        right_ear="Magnetic Earring",
    }

    sets.EnhancingDuration = {
        main={ name="Gada", augments={'Enh. Mag. eff. dur. +5','Mag. Acc.+17','"Mag.Atk.Bns."+5','DMG:+8',}},
        sub="Ammurapi Shield",
        head={ name="Telchine Cap", augments={'Enh. Mag. eff. dur. +10',}},
        body={ name="Telchine Chas.", augments={'Enh. Mag. eff. dur. +10',}},
        hands="Arbatel Bracers +3",
        legs={ name="Telchine Braconi", augments={'Enh. Mag. eff. dur. +10',}},
        feet={ name="Telchine Pigaches", augments={'Enh. Mag. eff. dur. +10',}},
        neck="Incanter's Torque",
        waist="Embla Sash",
        right_ear="Magnetic Earring",
        left_ring="Stikini Ring +1",
        right_ring="Stikini Ring +1",
    }

    sets.Nuking = {
        main="Bunzi's Rod",
        sub="Ammurapi Shield",
        ammo={ name="Ghastly Tathlum +1", augments={'Path: A',}},
        head={ name="Agwu's Cap", augments={'Path: A',}},
        body="Arbatel Gown +2",
        hands={ name="Agwu's Gages", augments={'Path: A',}},
        legs="Agwu's Slops",
        feet="Arbatel Loafers +3",
        neck={ name="Argute Stole +2", augments={'Path: A',}},
        waist="Hachirin-no-Obi",
        left_ear="Regal Earring",
        right_ear="Malignance Earring",
        left_ring={ name="Metamor. Ring +1", augments={'Path: A',}},
        right_ring="Freke Ring",
        back={ name="Lugh's Cape", augments={'INT+20','Mag. Acc+20 /Mag. Dmg.+20','INT+10','"Mag.Atk.Bns."+10',}},
    }

    sets.Helix = {
        main="Bunzi's Rod",
        sub="Ammurapi Shield",
        ammo={ name="Ghastly Tathlum +1", augments={'Path: A',}},
        head={ name="Agwu's Cap", augments={'Path: A',}},
        body={ name="Agwu's Robe", augments={'Path: A',}},
        hands={ name="Agwu's Gages", augments={'Path: A',}},
        legs="Arbatel Pants +2",
        feet="Arbatel Loafers +3",
        neck={ name="Argute Stole +2", augments={'Path: A',}},
        waist={ name="Acuity Belt +1", augments={'Path: A',}},
        left_ear="Malignance Earring",
        right_ear={ name="Arbatel Earring", augments={'System: 1 ID: 1676 Val: 0','Mag. Acc.+8',}},
        left_ring="Mujin Band",
        right_ring="Freke Ring",
        back={ name="Lugh's Cape", augments={'INT+20','Mag. Acc+20 /Mag. Dmg.+20','INT+10','"Mag.Atk.Bns."+10',}},
    }

    sets.Cure = {
        main="Chatoyant Staff",
        sub="Enki Strap",
        ammo="Staunch Tathlum +1",
        head={ name="Kaykaus Mitra +1", augments={'MP+80','"Cure" spellcasting time -7%','Enmity-6',}},
        body="Arbatel Gown +2",
        hands={ name="Kaykaus Cuffs +1", augments={'MP+80','MND+12','Mag. Acc.+20',}},
        legs={ name="Kaykaus Tights +1", augments={'MP+80','MND+12','Mag. Acc.+20',}},
        feet={ name="Kaykaus Boots +1", augments={'MP+80','"Cure" spellcasting time -7%','Enmity-6',}},
        neck={ name="Loricate Torque +1", augments={'Path: A',}},
        waist="Hachirin-no-Obi",
        left_ear="Magnetic Earring",
        right_ear="Regal Earring",
        left_ring="Defending Ring",
        right_ring="Ephedra Ring",
        back={ name="Lugh's Cape", augments={'INT+20','Mag. Acc+20 /Mag. Dmg.+20','INT+10','"Mag.Atk.Bns."+10',}},
    }

    -- With Relic head caps at 90%
    -- 82% + 13%
    sets.FC = {
        main={ name="Musa", augments={'Path: C',}}, -- 10%
        sub="Enki Strap",
        ammo="Incantor Stone", -- 2%
        head="Peda. M.Board +3", -- Reduces Casting time and Spell Recast by a further 13%
        body="Agwu's Robe", -- 8%
        hands={ name="Agwu's Gages", augments={'Path: A',}}, -- 6%
        legs="Pinga Pants +1", -- 13%
        feet={ name="Peda. Loafers +3", augments={'Enhances "Stormsurge" effect',}}, -- 8%
        neck="Orunmila's Torque", -- 5%
        waist="Embla Sash", -- 5%
        left_ear="Malignance Earring", -- 4%
        right_ear="Loquac. Earring", -- 2%
        left_ring="Kishar Ring", -- 4%
        right_ring="Weatherspoon Ring", -- 5%
        back={ name="Lugh's Cape", augments={'"Fast Cast"+10',}}, -- 10%
    }
end

function precast(spell)
    if spell.cast_time then
        equip(sets.FC)
    end
end


HelixSpells = S{'Anemohelix, Anemohelix II, Cryohelix', 'Cryohelix II', 'Geohelix', 'Geohelix II', 'Hydrohelix', 'Hydrohelix II', 'Ionohelix', 'Ionohelix II', 'Luminohelix', 'Luminohelix II', 'Noctohelix', 'Noctohelix II', 'Pyrohelix', 'Pyrohelix II'}
function midcast(spell)
    if spell.skill == 'Elemental Magic' then
        if HelixSpells:contains(spell.name) then
            equip(sets.Helix)
        else
            equip(sets.Nuking)
        end
    elseif spell.skill == 'Enhancing Magic' then
        if spell.name == "Regen V" then
            equip(sets.Regen)
        else
            equip(sets.EnhancingDuration)
        end
    elseif spell.skill == "Healing Magic" then
        equip(sets.Cure)
    end
end

function aftercast(spell)
    equip(sets.Idle)
end

function status_change(new,old)
    if T{'Idle','Resting'}:contains(new) then
        equip(sets.Idle)
    end
end
