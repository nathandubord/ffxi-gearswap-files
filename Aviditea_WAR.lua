include('organizer-lib')

function get_sets()
    organizer_items = {
        reraise="Reraise earring",
        warp="Warp ring"
    }

    -- Basic sets
    sets.Idle = {
        main="",
        sub="",
		ammo="Staunch Tathlum",
		head="Hjarrandi Helm",
		body="Sakpata breastplate",
		hands="Sakpata's Gauntlets",
		legs="Sakpata's Cuisses",
		feet="Sakpata's leggings",
		neck="Loricate torque +1",
		waist={ name="Sailfi Belt +1", augments={'Path: A',}},
		left_ear="",
		right_ear="",
		left_ring="Defending Ring",
		right_ring="",
		back="Shadow Mantle",
    }

    TPMode = 'TP'
    sets.TP = {}
    sets.TP.TP = {
    main="Naegling",
    ammo={ name="Coiste Bodhar", augments={'Path: A',}},
    head="Hjarrandi Helm",
    body="Sakpata's Plate",
    hands="Sakpata's Gauntlets",
    legs="Pumm. Cuisses +3",
    feet="Pumm. Calligae +3",
    neck={ name="War. Beads +2", augments={'Path: A',}},
    waist={ name="Sailfi Belt +1", augments={'Path: A',}},
    left_ear={ name="Schere Earring", augments={'Path: A',}},
    right_ear="Boii Earring",
    left_ring="Petrov Ring",
    right_ring="Rajas Ring",
    back={ name="Cichol's Mantle", augments={'DEX+20','Accuracy+20 Attack+20','Accuracy+10','"Dbl.Atk."+10','Damage taken-5%',}},
	}

	
	sets.TP.TwoHanded = {
    }


	sets.TP.Hybrid = {
}

 
    -- Job ability sets
    sets.JA = {}
    sets.JA.Aggressor = {
        head="Pummeler's Mask +1",
        body="Warrior's Lorica"
    }
    
    sets.JA.Warcry = {
        head="Agoge Mask"
    }
    
	sets.JA.Berserk = {
        feet="",
		body="Pummeler's lorica +2"
    }
    
    sets.JA.Restraint = {
        hands="Boii Mufflers +3"
    }

    sets.JA.Retaliation = {
        feet="",
		hands="Pummeler's mufflers +1"
    }

    sets.JA['Blood Rage'] = {
        body=""
    }

    sets.JA['Mighty Strikes'] = {
        hands=""
    }

    -- sets.JA.Berserk = {body="Pumm. Lorica +1", back={ name="Cichol's Mantle", augments={'STR+20','Accuracy+20 Attack+20','"Dbl.Atk."+10',}}, feet="Agoge Calligae +1"}
    -- sets.JA['Mighty Strikes'] = {hands="Agoge Mufflers +1"}
    -- sets.JA.Tomahawk = {ammo="Thr. Tomahawk",feet="Agoge Calligae +1"}
    -- sets.JA.Provoke = sets.Enmity

    -- Weaponskill sets
    sets.WS = {}
    sets.WS["Savage Blade"] = {
    main="Naegling",
    sub="Blurred Shield +1",
    ammo="Knobkierrie",
    head="Nyame Helm",
    body={ name="Nyame Mail", augments={'Path: B',}},
    hands="Boii Mufflers +2",
    legs={ name="Nyame Flanchard", augments={'Path: B',}},
    feet="Boii cuisses +3",
    neck={ name="War. Beads +2", augments={'Path: A',}},
    waist="Sailfi belt +1",
    left_ear="Thrud earring",
    right_ear="Ishvara Earring",
    left_ring={ name="Beithir Ring", augments={'Path: A',}},
    right_ring="Epaminondas's Ring",
    back={ name="Cichol's Mantle", augments={'STR+20','Accuracy+20 Attack+20','STR+10','Weapon skill damage +10%','Damage taken-5%',}},
    }
	
	sets.WS["Requiescat"] = {
        main="",
        sub="",
        ammo="knobkierrie",
        head="Agoge mask +3",
        neck="Fotia gorget",
        ear1="Thrud earring",
        ear2="Moonshade earring",
        body="Nyame Mail",
        hands="Boii mufflers +3",
        ring1="Cornelia's Ring",
        ring2="Sroda Ring",
        back={ name="Cichol's Mantle", augments={'STR+20','Accuracy+20 Attack+20','STR+10','Weapon skill damage +10%',}},
        waist="Fotia Belt",
        legs="Boii cuisses +3",
        feet="Nyame sollerets"
    }
	
	sets.WS["Circle Blade"] = {
        main="",
        sub="",
        ammo="knobkierrie",
        head="Agoge mask +3",
        neck="Fotia gorget",
        ear1="Cessance earring",
        ear2="Moonshade earring",
        body="Nyame Mail",
        hands="Sakpata's gauntlets",
        ring1="Flamma Ring",
        ring2="Petrov Ring",
        back={ name="Cichol's Mantle", augments={'STR+20','Accuracy+20 Attack+20','STR+10','Weapon skill damage +10%',}},
        waist="Fotia belt",
        legs="Sakpata's cuisses",
        feet="Nyame sollerets"
	}	
	
        sets.WS["Judgment"] = {
        main="",
        sub="",
        ammo="knobkierrie",
        head="Agoge mask +3",
        neck="Warrior's bead necklace +2",
        ear1="Thrud earring",
        ear2="Moonshade earring",
        body="Nyame Mail",
        hands="Boii mufflers +3",
        ring1="Cornelia's ring",
        ring2="Sroda Ring",
        back={ name="Cichol's Mantle", augments={'STR+20','Accuracy+20 Attack+20','STR+10','Weapon skill damage +10%',}},
        waist="Sailfi belt +1",
        legs="Boii cuisses +3",
        feet="Nyame sollerets"
	}
	
    sets.WS.Cataclysm = {
    main="Xoanon",
    sub="Utu Grip",
    ammo="Knobkierrie",
    head="Pixie Hairpin +1",
    body="Nyame Mail",
    hands="Nyame Gauntlets",
    legs="Nyame Flanchard",
    feet="Nyame Sollerets",
    neck={ name="War. Beads +2", augments={'Path: A',}},
    waist={ name="Sailfi Belt +1", augments={'Path: A',}},
    left_ear="Friomisi Earring",
    right_ear={ name="Moonshade Earring", augments={'Accuracy+4','TP Bonus +250',}},
    left_ring="Archon Ring",
    right_ring="Cornelia's Ring",
    back={ name="Cichol's Mantle", augments={'STR+20','Accuracy+20 Attack+20','STR+10','Weapon skill damage +10%',}},
}
	
	
	sets.WS["Fell Cleave"] = {
    main="",
    sub="",
    ammo="knobkierrie",
        head="Agoge mask +3",
        neck="Fotia gorget",
        ear1="Thrud earring",
        ear2="Moonshade earring",
        body="Nyame Mail",
        hands="Boii Mufflers +3",
        ring1="Cornelia's ring",
        ring2="Sroda Ring",
        back={ name="Cichol's Mantle", augments={'STR+20','Accuracy+20 Attack+20','STR+10','Weapon skill damage +10%',}},
        waist="Fotia belt",
        legs="Boii cuisses +3",
        feet="Nyame sollerets"
}
sets.WS["Ukko's Fury"] = {
        main="",
        sub="",
        ammo="Yetshila +1",
        head="Boii mask +3",
        neck="Warrior's bead necklace +2",
        ear1="Schere Earring",
        ear2="Boii earring +1",
        body="Hjarrandi breastplate",
        hands="Boii Mufflers +3",
        ring1="Begrudging Ring",
        ring2="Niqmaddu Ring",
        back={ name="Cichol's Mantle", augments={'STR+20','Accuracy+20 Attack+20','STR+10','Crit.hit rate+10','Damage taken-5%',}},
        waist={ name="Sailfi Belt +1", augments={'Path: A',}},
        legs="Boii cuisses +3",
        feet="Boii calligae +2"
	}
	
	    sets.WS["Steel Cyclone"] = {
        main="",
        sub="",
        ammo="knobkierrie",
        head="Agoge mask +3",
        neck="Warrior's bead necklace +2",
        ear1="Thrud earring",
        ear2="Moonshade earring",
        body="Nyame Mail",
        hands="Boii mufflers +3",
        ring1="Cornelia's Ring",
        ring2="Sroda Ring",
        back={ name="Cichol's Mantle", augments={'STR+20','Accuracy+20 Attack+20','STR+10','Weapon skill damage +10%',}},
        waist="Sailfi belt +1",
        legs="Boii cuisses +3",
        feet="Nyame sollerets"
    }
	
	
	
	sets.WS["Upheaval"] = {
        main="",
        sub="",
        ammo="knobkierrie",
        head="Agoge mask +2",
        neck="Warrior's bead necklace +2",
        ear1="Moonshade Earring",
        ear2="Thrud earring",
        body="Sakpata's breastplate",
        hands="Boii Mufflers +3",
        ring1="Sroda Ring",
        ring2="Niqmaddu Ring",
        back={ name="Cichol's Mantle", augments={'STR+20','Accuracy+20 Attack+20','STR+10','Weapon skill damage +10%',}},
        waist={ name="Sailfi Belt +1", augments={'Path: A',}},
        legs="Boii cuisses +3",
        feet="Boii calligae +2"
	}
			
    sets.WS["Resolution"] = {
        main="",
        sub="",
        ammo="knobkierrie",
        head="Agoge mask +3",
        neck="Fotia gorget",
        ear1="Thrud earring",
        ear2="Moonshade earring",
        body="Nyame Mail",
        hands={ name="Valorous Mitts", augments={'STR+10','Accuracy+23','Weapon skill damage +7%','Accuracy+3 Attack+3',}},
        ring1="Karieyh Ring",
        ring2="Niqmaddu Ring",
        back={ name="Cichol's Mantle", augments={'STR+20','Accuracy+20 Attack+20','STR+10','Weapon skill damage +10%',}},
        waist="Fotia belt",
        legs="Boii cuisses +2",
        feet="Nyame sollerets"
    }

sets.WS["Impulse Drive"] = {
        ammo="Yetshila +1",
        head="Boii mask +3",
        neck="Warrior's bead necklace +2",
        ear1="Moonshade Earring",
        ear2="Boii earring +1",
        body="Hjarrandi breastplate",
        hands="Boii Mufflers +3",
        ring1="Begrudging Ring",
        ring2="Niqmaddu Ring",
        back={ name="Cichol's Mantle", augments={'STR+20','Accuracy+20 Attack+20','STR+10','Crit.hit rate+10','Damage taken-5%',}},
        waist={ name="Sailfi Belt +1", augments={'Path: A',}},
        legs="Boii cuisses +3",
        feet="Boii calligae +2"
    }

sets.WS["Sonic Thrust"] = {
        main="",
        sub="",
        ammo="knobkierrie",
        head="Agoge mask +3",
        neck="Warrior's bead necklace +2",
        ear1="Thrud earring",
        ear2="Moonshade earring",
        body="Pummeler's lorica +3",
        hands="Boii mufflers +3",
        ring1="Cornelia's Ring",
        ring2="Sroda Ring",
        back={ name="Cichol's Mantle", augments={'STR+20','Accuracy+20 Attack+20','STR+10','Weapon skill damage +10%',}},
        waist="Sailfi belt +1",
        legs="Boii cuisses +3",
        feet="Nyame sollerets"
    }
end









function precast(spell)
    -- Not a mage, doesnt matter
end

-- Whenever you use a JA or a WS it will look for the set with that name
function midcast(spell, act)
    if sets.JA[spell.english] then
        equip(sets.JA[spell.english])
    elseif sets.WS[spell.english] then
        equip(sets.WS[spell.english])
    end
end

function aftercast(spell)
    if player.status == 'Engaged' then
        equip(sets.TP[TPMode])
    else
        equip(sets.Idle)
    end
end

function status_change(new, old)
    if new == 'Engaged' then
        equip(sets.TP[TPMode])
    else
        equip(sets.Idle)
    end
end

function self_command(command)
    if command == 'TP' then
        if TPMode =="TP" then
            TPMode ="TwoHanded"
        elseif TPMode =="TwoHanded" then
            TPMode ="Hybrid"
		elseif TPMode =="Hybrid" then
			TPMode ="TP"
        end
        windower.add_to_chat('TP mode is now: '..TPMode)
        equip(sets.TP[TPMode])
    end
end