-- Gear Sets
function get_sets()
    sets.WS = {}
    sets.WS['Savage Blade'] = {
        
    }

    sets.Idle = {
        ammo="Staunch Tathlum +1",
        head={ name="Nyame Helm", augments={'Path: B',}},
        body={ name="Nyame Mail", augments={'Path: B',}},
        hands={ name="Nyame Gauntlets", augments={'Path: B',}},
        legs={ name="Nyame Flanchard", augments={'Path: B',}},
        feet={ name="Nyame Sollerets", augments={'Path: B',}},
        neck={ name="Loricate Torque +1", augments={'Path: A',}},
        waist="Plat. Mog. Belt",
        right_ear="Eabani Earring",
        left_ring="Defending Ring",
        right_ring="Shneddick Ring",
        back="Shadow Mantle",
    }

    sets.TP = {
        ammo={ name="Coiste Bodhar", augments={'Path: A',}},
        head="Malignance Chapeau",
        body="Malignance Tabard",
        hands="Malignance Gloves",
        legs="Malignance Tights",
        feet="Malignance Boots",
        neck={ name="Loricate Torque +1", augments={'Path: A',}},
        waist="Reiki Yotai",
        left_ear="Telos Earring",
        right_ear="Suppanomimi",
        left_ring="Epona's Ring",
        right_ring="Defending Ring",
    }

    sets.FC = {
        ammo="Sapience Orb",
        head={ name="Herculean Helm", augments={'Attack+19','Pet: STR+1','Weapon skill damage +6%','Accuracy+14 Attack+14','Mag. Acc.+17 "Mag.Atk.Bns."+17',}},
        legs="Aya. Cosciales +1",
        left_ring="Kishar Ring",
        right_ring="Naji's Loop",
    }

    sets.Nuke = {
        main="Naegling",
        sub="Bunzi's Rod",
        head={ name="Nyame Helm", augments={'Path: B',}},
        body={ name="Nyame Mail", augments={'Path: B',}},
        hands={ name="Nyame Gauntlets", augments={'Path: B',}},
        legs={ name="Nyame Flanchard", augments={'Path: B',}},
        feet={ name="Nyame Sollerets", augments={'Path: B',}},
        neck="Incanter's Torque",
        waist="Eschan Stone",
        left_ear="Crep. Earring",
        right_ear="Friomisi Earring",
        right_ring="Kishar Ring",
    }
end

function precast(spell)
	if spell.type:contains('Magic') or spell.type:contains('Ninjutsu') then
        equip(sets.FC)
    elseif spell.type == "WeaponSkill" then
        equip(sets.WS[spell.english])
    end
end

function midcast(spell)
    if spell.english == "Magic Fruit" then
        equip(sets.Cure)
    elseif spell.english == "Tenebral Crush" or spell.english == "Firespit" or spell.english == "Anvil Lightning" or spell.english == "Entomb" or spell.english == "Spectral Floe" or spell.english == "Subduction" or spell.english == "Anvil Lightning" or spell.english == "Magic Hammer" or spell.english == "Death Ray" or spell.english == "Rail Cannon" or spell.english == "Regurgutation" then
        equip(sets.Nuke)
    elseif spell.english == "Sheep song" or spell.english == "Feather Tickle" or spell.english == "Absorb-TP" or spell.english == "Reaving Wind" or spell.english == "Blank Gaze" or spell.english == "Ice Break" or spell.english == "Dream Flower" or spell.english == "Cruel Joke" or spell.english == "Silent Storm" or spell.english == "Paralyzing Triad" or spell.english == "Sudden Lunge" then
        equip(sets.MagicAccuracy)
    elseif spell.english == "Occultation" then
        equip(sets.BlueMagicSkill)
    end
end

function aftercast(spell)
    if player.status == 'Engaged' then
        equip(sets.TP)
    else
        equip(sets.Idle)
    end
end

function status_change(new,old)
    if T{'Idle','Resting'}:contains(new) then
        equip(sets.IdleMode)
    elseif new == 'Engaged' then
        equip(sets.TP)
    end
end

