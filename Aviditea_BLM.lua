function get_sets()
    sets.Idle = {
        main="Malignance Pole",
        sub="Enki Strap",
        head="Inyanga Tiara +2",
        body="Shamash Robe",
        hands="Inyan. Dastanas +2",
        legs="Inyanga Shalwar +2",
        feet="Herald's Gaiters",
        neck="Loricate Torque +1",
        waist="Plat. Mog. Belt",
        left_ear="Eabani Earring",
        right_ear={ name="Moonshade Earring", augments={'HP+25','Latent effect: "Refresh"+1',}},
        left_ring="Defending Ring",
        right_ring="Gurebu's Ring",
    }

    sets.precast = {}

    sets.precast.FC = {
        ammo="Sapience Orb",
        head="Nahtirah Hat",
        body="Shango Robe",
        feet="Regal Pumps +1",
        waist="Embla Sash",
        right_ear="Malignance Earring",
        left_ring="Kishar Ring",
        right_ring="Lebeche Ring",
    }

    sets.midcast = {};
    sets.midcast.Nuking = {
        main="Bunzi's Rod",
        sub="Culminus",
        head="Jhakri Coronal +1",
        body={ name="Nyame Mail", augments={'Path: B',}},
        hands={ name="Nyame Gauntlets", augments={'Path: B',}},
        legs="Jhakri Slops +1",
        feet="Jhakri Pigaches +1",
        neck="Sibyl Scarf",
        waist="Eschan Stone",
        left_ear="Malignance Earring",
        right_ear="Hecate's Earring",
        left_ring="Stikini Ring",
        right_ring="Stikini Ring",
    }
end

function precast(spell)
    if spell.action_type == 'Magic' then
        equip(sets.precast.FC)
    end
end

function midcast(spell)
    if spell.skill =='Elemental Magic' then
            equip(sets.midcast.Nuking)
    end
end

function aftercast(spell)
    equip(sets.Idle)
end

function status_change(new, old)
    equip(sets.Idle)
end
