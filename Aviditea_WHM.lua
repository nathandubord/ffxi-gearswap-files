function get_sets()
    sets.Idle = {
        main="Malignance Pole",
        sub="Enki Strap",
        ammo="Homiliary",
        head="Inyanga Tiara +2",
        body="Shamash Robe",
        hands="Inyan. Dastanas +2",
        legs="Inyanga Shalwar +2",
        feet="Herald's Gaiters",
        neck="Loricate Torque +1",
        waist="Plat. Mog. Belt",
        left_ear="Eabani Earring",
        right_ear={ name="Moonshade Earring", augments={'HP+25','Latent effect: "Refresh"+1',}},
        left_ring="Defending Ring",
        right_ring="Gurebu's Ring",
        back={ name="Alaunus's Cape", augments={'MND+20','Eva.+20 /Mag. Eva.+20','Mag. Evasion+10','"Fast Cast"+10','Mag. Evasion+15',}},
    }

    sets.TP = {
        main="Tishtrya",
        sub="Culminus",
        ammo="Hasty Pinion +1",
        head="Nyame helm",
        body="Nyame Mail",
        hands="Bunzi's Gloves",
        legs="Nyame Flanchard",
        feet="Nyame Sollerets",
        waist="Eschan Stone",
        left_ear="Enervating Earring",
        right_ear="Steelflash Earring",
        left_ring="Defending Ring",
        right_ring="Raja's Ring",
    }

    sets.precast = {}

    -- 20% from /rdm FC 3 trait
    -- 62% from gear
    sets.precast.FC = {
        head="Nahtirah hat", -- FC 10%
        neck="Orison locket", -- FC 5%
        body="Inyanga Jubbah +2", -- 14%
        hands="Fanatic Gloves", -- FC 4%
        waist="Embla Sash", -- FC 5%
        ear1="Malignance earring", -- FC 4%
        back={ name="Alaunus's Cape", augments={'MND+20','Eva.+20 /Mag. Eva.+20','Mag. Evasion+10','"Fast Cast"+10','Mag. Evasion+15',}}, -- FC 10%
        legs="Aya. Cosciales +2", -- FC 6%
        feet="Regal pumps +1", -- FC 4%
        right_ring="Kishar ring", -- FC 5%
    }

    sets.midcast = {};
    sets.midcast.Cure = {
        main="Queller Rod", -- 10% CP, 2% CP2
        sub="Thuellaic Ecu +1",
        head="Vanya hood", -- 17% CP
        neck="Cleric's torque", -- 5% CP
        ear1="Magnetic Earring",
        ear2="Mendicant's Earring", -- 5% CP
        body="Ebers Bliaut +3",
        hands="Theophany mitts +3", -- 4% CP2
        ring1="Janniston Ring", -- 5% CP2
        ring2="Naji's loop", -- 1% CP, 1% CP2
        waist="Austerity belt",
        legs="Ebers pantaloons +3",
        feet="Vanya clogs", -- 12% CP
        back={ name="Alaunus's Cape", augments={'MND+20','Eva.+20 /Mag. Eva.+20','Mag. Evasion+10','"Fast Cast"+10','Mag. Evasion+15',}},
    }

    sets.midcast.Curaga = {
        main="Queller Rod", -- 10% CP, 2% CP2
        sub="Thuellaic Ecu +1",
        head="Vanya hood", -- 17% CP
        neck="Cleric's torque", -- 5% CP
        ear1="Magnetic Earring",
        ear2="Mendicant's Earring", -- 5% CP
        body="Theo. Bliaut +3", -- 3% CP2
        hands="Theophany mitts +3", -- 4% CP2
        ring1="Janniston Ring", -- 5% CP2
        ring2="Naji's loop", -- 1% CP, 1% CP2
        waist="Austerity belt",
        legs="Ebers pantaloons +3",
        feet="Vanya clogs", -- 12% CP
        back={ name="Alaunus's Cape", augments={'MND+20','Eva.+20 /Mag. Eva.+20','Mag. Evasion+10','"Fast Cast"+10','Mag. Evasion+15',}},
    }

    sets.midcast.Cursna = {
        main={ name="Gada", augments={'Enh. Mag. eff. dur. +4','Mag. Acc.+25','DMG:+11',}},
        sub="Culminus",
        ammo="Hasty Pinion +1",
        head="Inyanga Tiara +2",
        body="Ebers Bliaut +3",
        hands={ name="Fanatic Gloves", augments={'MP+40','Healing magic skill +4','"Conserve MP"+3','"Fast Cast"+4',}},
        legs="Th. Pantaloons +3",
        feet={ name="Vanya Clogs", augments={'MP+50','"Cure" potency +7%','Enmity-6',}},
        neck="Loricate Torque +1",
        waist="Bishop's Sash",
        left_ear={ name="Ebers Earring", augments={'System: 1 ID: 1676 Val: 0','Accuracy+6','Mag. Acc.+6',}},
        right_ear="Meili Earring",
        left_ring="Haoma's Ring",
        right_ring="Menelaus's Ring",
        back={ name="Alaunus's Cape", augments={'MND+20','Eva.+20 /Mag. Eva.+20','Mag. Evasion+10','"Fast Cast"+10','Mag. Evasion+15',}},
    }

    sets.midcast.Enfeebling = {
        main={ name="Gada", augments={'Enh. Mag. eff. dur. +4','Mag. Acc.+25','DMG:+11',}},
        sub="Thuellaic Ecu +1",
        ammo="Hydrocera",
        head="Ebers cap +3",
        body="Ebers Bliaut +3",
        hands="Theophany Mitts +3",
        legs="Ebers Pantaloons +3",
        feet="Theo. Duckbills +3",
        neck="Erra Pendant",
        waist="Eschan Stone",
        left_ear="Hermetic Earring",
        right_ear="Malignance Earring",
        left_ring="Stikini Ring",
        right_ring="Stikini Ring",
        back="Swith Cape",
    }

    sets.midcast.EnhancingSkill = {
        main={ name="Gada", augments={'Enh. Mag. eff. dur. +4','Mag. Acc.+25','DMG:+11',}},
        sub="Thuellaic Ecu +1",
        ammo="Hasty Pinion +1",
        head="Befouled Crown",
        body={ name="Telchine Chas.", augments={'Enh. Mag. eff. dur. +10',}},
        hands="Inyan. Dastanas +2",
        legs={ name="Piety Pantaln. +3", augments={'Enhances "Afflatus Misery" effect',}},
        feet="Ebers Duckbills +3",
        waist="Embla Sash",
        left_ring="Stikini Ring",
        right_ring="Stikini Ring",
        back="Fi Follet Cape",
    }

    sets.midcast.EnhancingDuration = set_combine(sets.midcast.EnhancingSkill, {
        main={ name="Gada", augments={'Enh. Mag. eff. dur. +4','Mag. Acc.+25','DMG:+11',}},
        head={ name="Telchine Cap", augments={'Enh. Mag. eff. dur. +10',}},
        body={ name="Telchine Chas.", augments={'Enh. Mag. eff. dur. +10',}},
        hands={ name="Telchine Gloves", augments={'Enh. Mag. eff. dur. +10',}},
        legs={ name="Telchine Braconi", augments={'Enh. Mag. eff. dur. +10',}},
        feet="Theo. Duckbills +3",
        waist="Embla Sash",
    })

    sets.midcast.Regen = set_combine(sets.midcast.EnhancingDuration, {
        head="Inyanga Tiara +2",
        hands="Ebers Mitts +3",
        legs="Th. Pantaloons +2",
    })

    sets.midcast.Recast = {
        ammo="Hasty Pinion +1",
        head="Inyanga Tiara +2",
        body="Ayanmo Corazza +2",
        hands="Inyan. Dastanas +2",
        legs="Aya. Cosciales +2",
        feet="Nyame sollerets",
    }

    sets.midcast.Erase = set_combine(sets.midcast.Recast, {
        neck="Cleric's torque",
        head="Ebers cap +3",
    })

    sets.midcast.BarSpells = set_combine(sets.midcast.EnhancingSkill, {
        main="Beneficus",
        head="Ebers cap +3",
        body="Ebers Bliaut +3",
        hands="Ebers Mitts +3",
        legs={ name="Piety Pantaln. +3", augments={'Enhances "Afflatus Misery" effect',}},
        feet="Ebers Duckbills +3",
    })

    sets.midcast.Auspice = set_combine(sets.midcast.EnhancingDuration, {
        feet="Ebers Duckbills +3"
    })

    sets.midcast.Nuking = {
        main="Daybreak",
        sub="Culminus",
        head="Bunzi's Hat",
        neck="Sibyl Scarf",
        body="Bunzi's Robe",
        hands="Bunzi's Gloves",
        legs="Bunzi's Pants",
        feet="Bunzi's Sabots",
        waist="Eschan Stone",
        left_ear="Hecate's Earring",
        right_ear="Malignance Earring",
        left_ring="Stikini Ring",
        right_ring="Stikini Ring",
        back="Searing Cape",
    }
end

Banished = S{'Banish','Banish II','Banish III','Banishga','Banishga II', 'Holy', 'Holy II'}
Barspells = S{'Barfira','Barfire','Barwater','Barwatera','Barstone','Barstonra','Baraero','Baraera','Barblizzara','Barblizzard','Barthunder', 'Barthundra', 'Barparalyze', 'Barparalyzra', 'Barsleep', 'Barsleepra', 'Barpoison', 'Barpoisonra', 'Barblind', 'Barblindra', 'Barsilence', 'Barsilencera', 'Barpetrify', 'Barpetra', 'Barvirus', 'Barvira'}
Boosts = S{'Boost-AGI', 'Boost-CHR', 'Boost-DEX', 'Boost-INT', 'Boost-MND', 'Boost-STR', 'Boost-VIT'}
Cures = S{'Cure','Cure II','Cure III','Cure IV','Cure V','Cure VI'}
Curagas = S{'Curaga','Curaga II','Curaga III','Curaga IV','Curaga V','Cura','Cura II','Cura III'}
Removals = S{'Paralyna','Silena','Viruna', 'Stona','Blindna','Poisona'}
Raises = S{'Raise', 'Raise II', 'Raise III', 'Arise', 'Reraise', 'Reraise II', 'Reraise III', 'Reraise IV'}
Regens = S{'Regen','Regen II','Regen III','Regen IV','Regen V'}

function precast(spell)
    if spell.action_type == 'Magic' then
        equip(sets.precast.FC)
    end
end

function midcast(spell)
    if spell.skill =='Healing Magic' then
        if Cures:contains(spell.name) then
            equip(sets.midcast.Cure)
        elseif Curagas:contains(spell.name) then
            equip(sets.midcast.Curaga)
        elseif Removals:contains(spell.name) then
            equip(sets.midcast.Erase)
        elseif Raises:contains(spell.name) then
            equip(sets.midcast.Recast)
        elseif spell.name == 'Cursna' then
            equip(sets.midcast.Cursna)
        end
    elseif spell.skill =='Enhancing Magic' then
        if Barspells:contains(spell.name) then
            equip(sets.midcast.BarSpells)
        elseif Regens:contains(spell.name) then
            equip(sets.midcast.Regen)
        elseif Boosts:contains(spell.name) then
            equip(sets.midcast.Boost)
        elseif spell.name == "Auspice" then
            equip(sets.midcast.Auspice)
        elseif spell.name == 'Erase' then
            equip(sets.midcast.Erase)
        else
            equip(sets.midcast.EnhancingDuration)
        end
    elseif spell.skill =='Divine Magic' then
        equip(sets.midcast.Nuking)
    elseif spell.skill =='Enfeebling Magic' then
        equip(sets.midcast.Enfeebling)
    end
end

function aftercast(spell)
    if player.status == 'Engaged' then
        equip(sets.TP)
    else
        equip(sets.Idle)
    end
end

function status_change(new, old)
    if new == 'Engaged' then
        equip(sets.TP)
    else
        equip(sets.Idle)
    end
end

-- windower.register_event('chat message', function(message, sender, mode)
--     -- Debug message to check mode and message
--     windower.add_to_chat(207, "Received message: " .. message)

--     -- Check if the incoming message is in party chat (mode 3) and contains "warp"
--     if mode == 4 and string.find(message:lower(), "warp") then
--         -- Execute the warp command
--         windower.send_command('input /ma Warp II ' .. sender)
--     elseif mode == 4 and string.find(message:lower(), "follow") then
--         windower.send_command('input /follow ' .. sender)
--     end
-- end)