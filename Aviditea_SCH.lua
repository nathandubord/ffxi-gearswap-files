function get_sets()
    sets.Idle = {
        main="Malignance pole",
        sub="Enki Strap",
        ammo="Homiliary",
        head="Nyame Helm",
        body="Shamash Robe",
        hands="Nyame Gauntlets",
        legs="Nyame Flanchard",
        feet="Herald's Gaiters",
        neck="Loricate Torque +1",
        waist="Carrier's sash",
        left_ear="Eabani Earring",
        right_ear={ name="Moonshade Earring", augments={'HP+25','Latent effect: "Refresh"+1',}},
        left_ring="Defending Ring",
        right_ring="Stikini Ring",
        back="Shadow Mantle",
    }

    sets.FC = {
        ammo="Sapience Orb",
        head="Nahtirah Hat",
        body="Shango Robe",
        feet="Regal Pumps +1",
        waist="Embla Sash",
        left_ear="Malignance Earring",
        left_ring="Menelaus's Ring",
        right_ring="Kishar Ring",
    }

    sets.midcast = {};
    sets.midcast.Cure = {
        main="Daybreak", -- 30%
        sub="Thuellaic Ecu +1",
        amm="",
        head="Vanya hood", -- 17% CP
        ear1="Magnetic Earring",
        ear2="Mendicant's Earring", -- 5% CP
        body="",
        hands="",
        ring1="Janniston Ring", -- 5% CP2
        ring2="Naji's loop", -- 1% CP, 1% CP2
        waist="Austerity belt",
        legs="",
        feet="Vanya clogs", -- 12% CP
        back="Fi Follet Cape",
    }

    sets.midcast.EnhancingSkill = {
        main={ name="Gada", augments={'Enh. Mag. eff. dur. +4','Mag. Acc.+25','DMG:+11',}},
        sub="Thuellaic Ecu +1",
        ammo="Hasty Pinion +1",
        head="Befouled Crown",
        body="",
        hands="",
        legs="",
        feet="",
        waist="Embla Sash",
        left_ring="Stikini Ring",
        right_ring="Stikini Ring",
        back="Fi Follet Cape",
    }

    sets.midcast.EnhancingDuration = set_combine(sets.midcast.EnhancingSkill, {
        main={ name="Gada", augments={'Enh. Mag. eff. dur. +4','Mag. Acc.+25','DMG:+11',}},
        head={ name="Telchine Cap", augments={'Enh. Mag. eff. dur. +9',}},
        body={ name="Telchine Chas.", augments={'Enh. Mag. eff. dur. +10',}},
        hands={ name="Telchine Gloves", augments={'Enh. Mag. eff. dur. +9',}},
        legs={ name="Telchine Braconi", augments={'Enh. Mag. eff. dur. +8',}},
        feet="Telchine pigaches",
        waist="Embla Sash",
    })
end

function precast(spell)
    if spell.action_type == 'Magic' then
        equip(sets.FC)
    end
end

function midcast(spell)
    if spell.skill == 'Healing Magic' then
        equip(sets.midcast.Cure)
    elseif spell.skill == 'Enhancing Magic' then
        equip(sets.midcast.EnhancingDuration)
    end
end

function aftercast(spell)
    equip(sets.Idle)
end

function status_change(new, old)
    equip(sets.Idle)
end
