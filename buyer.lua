_addon.name     = "Buyer"
_addon.version  = "1"
_addon.author   = "Newty"
_addon.commands = {"Buyer"}

require("coroutine")
require("logger")

-- Function to check if the inventory is full
local function is_inventory_full()
    return windower.ffxi.get_bag_info().inventory.max <= windower.ffxi.get_bag_info().inventory.count;
end

local function execute_key_press(key)
    windower.send_command('setkey ' .. key .. ' down')
    coroutine.sleep(0.1)
    windower.send_command('setkey ' .. key .. ' up')
    coroutine.sleep(0.1)
end

local continue_execution = true  -- Global variable to control loop execution

-- Function to execute the sequence of commands
local function execute_sequence()
    log("Buyer: Buying a Musketeer Gun")

    -- Lock onto NPC
    windower.send_command('input /targetnpc')
    execute_key_press('h')
    coroutine.sleep(0.2)
    -- Select buy with conquest points
    execute_key_press('enter')
    coroutine.sleep(1.7)
    execute_key_press('down')
    execute_key_press('enter')
    coroutine.sleep(0.5)

    -- Select 16k
    execute_key_press('right')
    execute_key_press('down')
    execute_key_press('down')
    execute_key_press('enter')
    coroutine.sleep(0.5)

    -- Select the gun
    execute_key_press('right')
    execute_key_press('down')
    execute_key_press('down')
    execute_key_press('enter')
    coroutine.sleep(1.2)

    -- Close gun preview
    execute_key_press('enter')
    coroutine.sleep(0.7)

    -- Confirm purchase
    execute_key_press('up')
    execute_key_press('enter')
    coroutine.sleep(1.5)
end

windower.register_event("addon command", function (...)
    local user_input = {...}
    local command = table.remove(user_input, 1):lower()

    if command == "start" then
        continue_execution = true
        log("Buyer: Started")
        while continue_execution do
            if is_inventory_full() then
                log("Buyer: Inventory is full. Stopping.")
                break
            end
            execute_sequence()
        end
        log("Buyer: Stopped")
    elseif command == "stop" then
        log("Buyer: Stopping after next purchase")
        continue_execution = false
    else
        log("Unknown command.")
    end
end)
