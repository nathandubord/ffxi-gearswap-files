-- Gear Sets
function get_sets()
    -- Sets that have multiple variations
    sets.JA = {}
    sets.FC = {}
    sets.WS = {}
    sets.Idle = {}

    -- Modes and their default values
    IdleMode = 'Default'

    -- Attacks: 3
    -- Modifier: 80% DEX
    -- fTP: 1.6328125
    sets.WS['Chant du Cygne'] = {
        ammo="Honed Tathlum",
        head={ name="Adhemar Bonnet +1", augments={'STR+12','DEX+12','Attack+20',}},
        body="Gleti's Cuirass",
        hands="Gleti's Gauntlets",
        legs="Gleti's Breeches",
        feet="Gleti's Boots",
        neck="Fotia Gorget",
        waist="Fotia Belt",
        left_ear="Odr Earring",
        right_ear="Mache Earring +1",
        left_ring="Epona's Ring",
        right_ring="Begrudging Ring",
        back={ name="Rosmerta's Cape", augments={'DEX+20','Accuracy+20 Attack+20','DEX+10','Crit.hit rate+10',}},
    }

    sets.WS['Judgement'] = {
        ammo="Honed Tathlum",
        head={ name="Adhemar Bonnet +1", augments={'STR+12','DEX+12','Attack+20',}},
        body={ name="Adhemar Jacket +1", augments={'DEX+12','AGI+12','Accuracy+20',}},
        hands={ name="Adhemar Wrist. +1", augments={'STR+12','DEX+12','Attack+20',}},
        legs={ name="Samnuha Tights", augments={'STR+9','DEX+8','"Dbl.Atk."+2','"Triple Atk."+2',}},
        feet={ name="Herculean Boots", augments={'Attack+28','"Triple Atk."+4','STR+1',}},
        neck="Fotia Gorget",
        waist="Fotia Belt",
        left_ear="Odr Earring",
        right_ear="Mache Earring +1",
        left_ring="Epona's Ring",
        right_ring="Begrudging Ring",
        back={ name="Rosmerta's Cape", augments={'DEX+20','Accuracy+20 Attack+20','DEX+10','Crit.hit rate+10',}},
    }

    sets.WS['Black Halo'] = {
        ammo="Honed Tathlum",
        head="Nyame Helm",
        body="Nyame Mail",
        hands="Jhakri Cuffs +2",
        legs="Nyame Flanchard",
        feet="Nyame Sollerets",
        neck="Fotia Gorget",
        waist="Orpheus's Sash",
        left_ear={ name="Moonshade Earring", augments={'Accuracy+4','TP Bonus +250',}},
        right_ear="Mache Earring +1",
        left_ring="Stikini Ring +1",
        right_ring="Stikini Ring +1",
        back={ name="Rosmerta's Cape", augments={'DEX+20','Accuracy+20 Attack+20','DEX+10','Crit.hit rate+10',}},
    }

    sets.CDC = sets.WS['Chant du Cygne'];

    sets.WS['Savage Blade'] = {
        main="Naegling",
        sub={ name="Thibron", augments={'TP Bonus +1000',}},
        ammo="Oshasha's Treatise",
        head={ name="Nyame Helm", augments={'Path: B',}},
        body={ name="Nyame Mail", augments={'Path: B',}},
        hands={ name="Nyame Gauntlets", augments={'Path: B',}},
        legs={ name="Nyame Flanchard", augments={'Path: B',}},
        feet={ name="Nyame Sollerets", augments={'Path: B',}},
        neck={ name="Mirage Stole +2", augments={'Path: A',}},
        waist={ name="Sailfi Belt +1", augments={'Path: A',}},
        left_ear={ name="Moonshade Earring", augments={'Accuracy+4','TP Bonus +250',}},
        right_ear="Ishvara Earring",
        left_ring="Sroda Ring",
        right_ring="Cornelia's Ring",
        back={ name="Rosmerta's Cape", augments={'STR+20','Accuracy+20 Attack+20','STR+10','Weapon skill damage +10%',}},
    }

    sets.WS['Circle Blade'] = {
        ammo="Honed Tathlum",
        head={ name="Adhemar Bonnet +1", augments={'STR+12','DEX+12','Attack+20',}},
        body={ name="Adhemar Jacket +1", augments={'DEX+12','AGI+12','Accuracy+20',}},
        hands={ name="Adhemar Wrist. +1", augments={'STR+12','DEX+12','Attack+20',}},
        legs={ name="Samnuha Tights", augments={'STR+9','DEX+8','"Dbl.Atk."+2','"Triple Atk."+2',}},
        feet={ name="Herculean Boots", augments={'Attack+28','"Triple Atk."+4','STR+1',}},
        neck="Fotia Gorget",
        waist="Fotia Belt",
        left_ear="Steelflash Earring",
        right_ear="Bladeborn Earring",
        left_ring="Epona's Ring",
        right_ring="Begrudging Ring",
        back={ name="Rosmerta's Cape", augments={'DEX+30','Accuracy+20 Attack+20','DEX+1','Crit.hit rate+10',}},
    }

    sets.TP = {
        main="Naegling",
        sub={ name="Thibron", augments={'TP Bonus +1000',}},
        ammo={ name="Coiste Bodhar", augments={'Path: A',}},
        head="Malignance Chapeau",
        body="Malignance Tabard",
        hands="Malignance Gloves",
        legs="Malignance Tights",
        feet="Malignance Boots",
        neck={ name="Mirage Stole +2", augments={'Path: A',}},
        waist="Reiki Yotai",
        left_ear="Suppanomimi",
        right_ear="Telos Earring",
        left_ring="Chirich Ring +1",
        right_ring="Defending Ring",
        back={ name="Rosmerta's Cape", augments={'DEX+20','Accuracy+20 Attack+20','Accuracy+10','"Store TP"+10','Phys. dmg. taken-10%',}},
    }

    -- DT: 49%
    -- PDT: 53%
    -- MDT: 49%
    sets.DT = {
        main="Almace",
        sub="Colada",
        ammo="Staunch tathlum +1",
        head="Nyame Helm",
        body="Hashishin mintan +2",
        hands="Nyame Gauntlets",
        legs="Malignance tights",
        feet="Malignance boots",
        neck={ name="Loricate Torque +1", augments={'Path: A',}},
        waist="Platinum moogle belt",
        left_ear="Genmei Earring",
        right_ear="Hearty Earring",
        left_ring="Chirich Ring +1",
        right_ring="Defending Ring",
        back="Shadow Mantle",
    }

    -- Typical all purpose idle set
    sets.Idle.Default = set_combine(sets.DT, {
        legs="Carmine Cuisses +1",
        left_ring="Stikini Ring +1"
    })

    sets.Idle.DT = set_combine(sets.DT, {
        legs="Carmine Cuisses +1",
    })

    sets.Idle.Evasion = set_combine(sets.DT, {
        main="Sakpata's Sword",
        sub="Ephemeron",
        ammo="Amar Cluster",
        head="Malignance Chapeau",
        body="Malignance Tabard",
        hands="Malignance Gloves",
        legs="Carmine cuisses +1",
        feet="Malignance boots",
        neck={ name="Bathy Choker +1", augments={'Path: A',}},
        waist="Svelt. Gouriz +1",
        left_ear="Eabani Earring",
        right_ear="Infused Earring",
        left_ring="Alert Ring",
        right_ring="Defending Ring",
        back={ name="Rosmerta's Cape", augments={'AGI+20','Eva.+20 /Mag. Eva.+20','Evasion+10','Evasion+15',}},
    })

    -- Fast Cast: 53%
    -- Insta Cast: 3%
    sets.FC.Standard = {
        ammo="Sapience orb", -- 2%
        head={ name="Carmine Mask +1", augments={'Accuracy+20','Mag. Acc.+12','"Fast Cast"+4',}}, -- 14%
        body="Hashishin mintan +2", -- 5%
        hands="Thaumas Gloves", -- 4%
        legs="Pinga pants +1", -- 6%
        feet="Carmine Greaves +1", -- 13%
        neck="Orunmila's Torque", -- 5%
        waist="Witful Belt", -- 3%
        left_ear="Loquac. Earring", -- 2%
        left_ring="Prolix Ring", -- 2%
        right_ring="Kishar Ring", -- 4%
        back="Swith Cape +1", -- 4%
    }

    -- 52% FC
    sets.FC.Blu = set_combine(sets.FC.Standard, {
        body="Hashishin mintan +2", -- 15%
    })

    sets.Cure = {
        ammo="Mavi Tathlum",
        head="Aya. Zucchetto +2",
        body="Hashishin mintan +2",
        hands="Hashi. Bazu. +1",
        legs="Hashishin Tayt +1",
        feet={ name="Medium's Sabots", augments={'MP+5','MND+5',}},
        neck="Mavi Scarf",
        waist="Chuq'aba Belt",
        left_ear="Lifestorm Earring",
        right_ear="Psystorm Earring",
        left_ring="Asklepian Ring",
        right_ring="Vocane Ring",
        back="Solemnity Cape",
    }

    sets.Nuke = {
        main="Maxentius",
        sub="Bunzi's Rod",
        ammo="Ghastly Tathlum +1",
        body="Cohort cloak +1",
        hands="Jhakri Cuffs +2",
        legs="Luhlaza Shalwar +3",
        feet="Jhakri Pigaches +2",
        neck="Sibyl Scarf",
        waist="Orpheus's Sash",
        left_ear="Regal Earring",
        right_ear="Friomisi Earring",
        left_ring="Strendu Ring",
        right_ring="Metamorph ring +1",
        back={ name="Rosmerta's Cape", augments={'INT+20','Mag. Acc+20 /Mag. Dmg.+20','Magic Damage +5','"Mag.Atk.Bns."+10',}},
    }

    sets.MagicAccuracy = {
        main="Maxentius",
        sub="Bunzi's Rod",
        ammo="Hydrocera",
        head="Malignance Chapeau",
        body="Hashishin Mintan +2",
        hands="Malignance Gloves",
        legs="Malignance Tights",
        feet="Malignance Boots",
        neck={ name="Mirage Stole +2", augments={'Path: A',}},
        waist={ name="Acuity Belt +1", augments={'Path: A',}},
        left_ear="Crep. Earring",
        right_ear={ name="Hashi. Earring +1", augments={'System: 1 ID: 1676 Val: 0','Accuracy+12','Mag. Acc.+12','"Dbl.Atk."+4',}},
        left_ring="Stikini Ring +1",
        right_ring={ name="Metamor. Ring +1", augments={'Path: A',}},
        back={ name="Rosmerta's Cape", augments={'INT+20','Mag. Acc+20 /Mag. Dmg.+20','Magic Damage +5','"Mag.Atk.Bns."+10',}},
    }

    sets.BlueMagicSkill = {
        ammo="Mavi tathlum",
        neck="Mavi scarf",
        body="Magus jubbah +1",
        hands="Ayao's Gages",
        back="Cornflower cape",
        legs="Hashishin tayt +1",
        feet="Luhlaza charuqs +3",
        left_ring="Stikini Ring +1",
        right_ring="Stikini Ring +1",
    }

    sets.Smithing = {
        sub="Smythe's Shield",
        head="Qiqirn Hood",
        body="Blksmith. Smock",
        hands="Smithy's Mitts",
        legs="Carmine Cuisses +1",
        neck="Smithy's Torque",
        left_ring="Craftmaster's Ring",
        right_ring="Craftkeeper's Ring",
    }

    sets.Diffusion = { feet="Luhlaza Charuqs +3" }
end

function precast(spell)
	if spell.type:contains('Magic') or spell.type:contains('Ninjutsu') then
        if spell.skill == 'Blue Magic' then
            equip(sets.FC.Blu)
        else
            equip(sets.FC.Standard)
        end
    elseif spell.type == "WeaponSkill" then
        equip(sets.WS[spell.english])
    end
end

function midcast(spell)
    if spell.english == "Magic Fruit" then
        equip(sets.Cure)
    elseif spell.english == "Tenebral Crush" or spell.english == "Firespit" or spell.english == "Anvil Lightning" or spell.english == "Entomb" or spell.english == "Spectral Floe" or spell.english == "Subduction" or spell.english == "Anvil Lightning" or spell.english == "Magic Hammer" or spell.english == "Death Ray" or spell.english == "Rail Cannon" or spell.english == "Regurgutation" then
        equip(sets.Nuke)
    elseif spell.english == "Sheep song" or spell.english == "Feather Tickle" or spell.english == "Absorb-TP" or spell.english == "Reaving Wind" or spell.english == "Blank Gaze" or spell.english == "Ice Break" or spell.english == "Dream Flower" or spell.english == "Cruel Joke" or spell.english == "Silent Storm" or spell.english == "Paralyzing Triad" or spell.english == "Sudden Lunge" then
        equip(sets.MagicAccuracy)
    elseif spell.english == "Occultation" then
        equip(sets.BlueMagicSkill)
    end

    if buffactive["Diffusion"] then
        equip(sets.Diffusion)
    end
end

function aftercast(spell)
    if player.status == 'Engaged' then
        equip(sets.TP)
    else
        equip(sets.Idle[IdleMode])
    end
end

function status_change(new,old)
    if T{'Idle','Resting'}:contains(new) then
        equip(sets.Idle[IdleMode])
    elseif new == 'Engaged' then
        equip(sets.TP)
    end
end

function self_command(command)
    if command == 'Idle' then
        if IdleMode =="Default" then
            IdleMode ="DT"
        elseif IdleMode =="DT" then
            IdleMode ="Evasion"
        elseif IdleMode =="Evasion" then
            IdleMode ="Default"
        end
        windower.add_to_chat('Idle mode is now: '..IdleMode)
        equip(sets.Idle[IdleMode])
    end
end
