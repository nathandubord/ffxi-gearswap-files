function get_sets()	
	Weapon = 'Burtgang'
	Shield = 'Aegis'

	sets.WS = {}
	sets.WS["Savage Blade"] = {
		ammo="Ginsen",
		head="Odyssean Helm",
		body="Sakpata's Plate",
		hands="Sakpata's Gauntletsa",
		legs="Sakpata's Cuisses",
		feet="Sulev. Leggings +2",
		neck="Fotia Gorget",
		waist="Fotia Belt",
		left_ear={ name="Moonshade Earring", augments={'"Mag.Atk.Bns."+4','TP Bonus +250',}},
		right_ear="Thrud earring",
		left_ring="Cornelia's Ring",
		right_ring="Epaminondas's Ring",
		back={ name="Rudianos's Mantle", augments={'VIT+20','Accuracy+20 Attack+20','Accuracy+10','"Dbl.Atk."+10','Spell interruption rate down-10%',}},
	}
	
	sets.WS['Aeolian Edge'] = {
		ammo="Oshasha's Treatise",
		head="Nyame Helm",
		body="Nyame Mail",
		hands="Nyame Gauntlets",
		legs="Nyame Flanchard",
		feet="Sulev. Leggings +2",
		neck="Fotia Gorget",
		waist="Fotia Belt",
		left_ear={ name="Moonshade Earring", augments={'"Mag.Atk.Bns."+4','TP Bonus +250',}},
		right_ear="Friomisi Earring",
		left_ring="Cornelia's Ring",
		right_ring="Karieyh Ring +1",
		back="Toro Cape",	
	}
	
	sets.Pulling = {
		ammo="Staunch Tathlum",
		head="Chev. Armet +3",
		body={ name="Sakpata's Plate", augments={'Path: A',}},
		hands={ name="Souv. Handsch. +1", augments={'HP+65','Shield skill +15','Phys. dmg. taken -4',}},
		legs="Chev. Cuisses +3",
		feet="Sakpata's Leggings",
		neck={ name="Loricate Torque +1", augments={'Path: A',}},
		waist="Plat. Mog. Belt",
		left_ear="Thureous Earring",
		right_ear={ name="Chev. Earring", augments={'System: 1 ID: 1676 Val: 0','Accuracy+6','Mag. Acc.+6',}},
		left_ring="Warden's Ring",
		right_ring="Fortified Ring",
		back={ name="Rudianos's Mantle", augments={'VIT+20','Accuracy+20 Attack+20','Accuracy+10','"Dbl.Atk."+10','Spell interruption rate down-10%',}},
	}
	
	sets.Ochain = {
		sub="Ochain",	
	}
	
	sets.Aegis = {
		sub="Aegis"
	}
	
	sets.Priwen ={
		sub="Priwen"
	}
	
	sets.TP = {
		ammo="Ginsen",
		head="Flam. Zucchetto +2",
		body="Sakpata's Plate",
		hands="Sakpata's Gauntlets",
		legs="Sakpata's Cuisses",
		feet="Flam. Gambieras +2",
		neck="Sanctity Necklace",
		waist="Sailfi Belt +1",
		left_ear="Cessance Earring",
		right_ear="Brutal Earring",
		left_ring="Defending Ring",
		right_ring="Chirich Ring",
		back={ name="Rudianos's Mantle", augments={'VIT+20','Accuracy+20 Attack+20','Accuracy+10','"Dbl.Atk."+10','Spell interruption rate down-10%',}},
	}
	
	sets.Naegling = {
		main="Naegling",
	}

	sets.Dagger = {
		main="Malevolence"
	}
	
	sets.Burtgang = {
		main="Burtgang"
	}
	
	sets.Town = set_combine(sets.Enmity, {
		legs="Carmine cuisses +1",
	})

    sets.Enmity = {
		ammo="Sapience Orb",
		head="Loess Barbuta",
		body={ name="Souv. Cuirass +1", augments={'HP+105','Enmity+9','Potency of "Cure" effect received +15%',}},
		hands={ name="Cab. Gauntlets +1", augments={'Enhances "Chivalry" effect',}},
		legs={ name="Souv. Diechlings +1", augments={'HP+105','Enmity+9','Potency of "Cure" effect received +15%',}},
		feet="Eschite Greaves",
		neck="Moonbeam Necklace",
		waist="Creed Baudrier",
		left_ear="Trux Earring",
		right_ear="Cryptic Earring",
		left_ring="Apeile Ring +1",
		right_ring="Apeile Ring",
		back={ name="Weard Mantle", augments={'VIT+4','DEX+3','Enmity+5','Phalanx +3',}},
    }
	
	sets.Fastcast = {
		ammo="Sapience Orb",
		head={ name="Carmine Mask", augments={'Accuracy+15','Mag. Acc.+10','"Fast Cast"+3',}},
		body="Odyss. Chestplate",
		hands="Regal Gauntlets",
		legs="Nyame Flanchard",
		feet={ name="Carmine Greaves", augments={'HP+60','MP+60','Phys. dmg. taken -3',}},
		neck="Orunmila's Torque",
		waist="Creed Baudrier",
		left_ear="Loquac. Earring",
		right_ear="Etiolation Earring",
		left_ring="Prolix Ring",
		right_ring="K'ayres Ring",
		back={ name="Weard Mantle", augments={'VIT+4','DEX+3','Enmity+5','Phalanx +3',}},
	}
	
	sets.SpellInteruptionDown = set_combine(sets.Enmity, {
		ammo="Staunch Tathlum",
		head={ name="Souv. Schaller +1", augments={'HP+105','VIT+12','Phys. dmg. taken -4',}},
		hands="Regal Gauntlets",
		legs={ name="Founder's Hose", augments={'MND+2','Mag. Acc.+4','Breath dmg. taken -1%',}},
		feet="Odyssean Greaves",
		back={ name="Rudianos's Mantle", augments={'VIT+20','Accuracy+20 Attack+20','Accuracy+10','"Dbl.Atk."+10','Spell interruption rate down-10%',}},
	})
	
	sets.Cure = {
		ammo="Staunch Tathlum",
		head={ name="Souv. Schaller +1", augments={'HP+105','VIT+12','Phys. dmg. taken -4',}},
		body={ name="Souv. Cuirass +1", augments={'HP+105','Enmity+9','Potency of "Cure" effect received +15%',}},
		hands="Regal Gauntlets",
		legs={ name="Founder's Hose", augments={'MND+2','Mag. Acc.+4','Breath dmg. taken -1%',}},
		feet="Odyssean Greaves",
		neck="Twilight Torque",
		waist="Creed Baudrier",
		left_ear="Nourish. Earring +1",
		right_ear="Odnowa Earring",
		left_ring="K'ayres Ring",
		right_ring="Vocane Ring +1",
		back={ name="Rudianos's Mantle", augments={'VIT+20','Accuracy+20 Attack+20','Accuracy+10','"Dbl.Atk."+10','Spell interruption rate down-10%',}},
	}
	
	sets.Phalanx = {
        head="Odyssean Helm",
	    hands={ name="Souv. Handsch. +1", augments={'HP+65','Shield skill +15','Phys. dmg. taken -4',}},
		body={ name="Odyssean Chestplate"},
		legs={ name="Sakpata's Cuisses"},
		feet={ name="Souveran Schuhs", augments={'HP+50','Attack+20','Magic dmg. taken -3'}},
		Back={ name="Weard Mantle"},
	}
	
	sets.JA = {}
	sets.JA.Provoke = set_combine(sets.Enmity)
	sets.JA.Invincible = set_combine(sets.Enmity, {legs="Cab. Breeches +2"})
	sets.JA.Sentinel = set_combine(sets.Enmity, {feet="Cab. Leggings +1"})
	sets.JA.Chivalry = set_combine(sets.Enmity, {hands="Cab. Gauntlets +1"})
	sets.JA.Rampart = set_combine(sets.Enmity, {head="Cab. Coronet +2"})
	sets.JA.Fealty = set_combine(sets.Enmity, {body="Cab. Surcoat +3"})
	sets['Divine Emblem'] = set_combine(sets.Enmity, {feet="Creed sabatons +1"})
	sets.JA.Palisade = sets.enmity
end


function precast(spell)
	if spell.type:contains('Magic') then
		equip(sets.Fastcast)	
	elseif sets.WS[spell.english] then
		if buffactive['terror'] or buffactive['petrified'] or buffactive['sleep'] or buffactive['stun'] or buffactive['amnesia'] or spell.target.distance > 6 then
			cancel_spell()
		else
			equip(sets.WS[spell.english])
		end
    elseif sets.JA[spell.english] then
		equip(sets.JA[spell.english])
	end
end

function midcast(spell)
	if spell.type:contains('Magic') then
		if spell.skill=='Healing Magic' then
			equip(sets.Cure)
		elseif sets[spell.english] then
			equip(sets.SpellInteruptionDown, set_combine(sets[spell.english])) 
		elseif spell.type:contains('Magic') then
			equip(sets.SpellInteruptionDown)
		end
	end
end

function aftercast(spell)
	if (player.status == 'Engaged') then
		equip(sets.TP)
	else
		equip(sets.Pulling)
	end
end

function status_change(new,old)
    if T{'Idle','Resting'}:contains(new) then
        equip(sets.Pulling)
    elseif new == 'Engaged' then
        equip(sets.TP)
    end
end

function self_command(command)
    if command == 'Weapon' then
        if Weapon == "Burtgang" then
            Weapon = "Naegling"
		elseif Weapon == "Naegling" then
            Weapon = "Dagger"
		elseif Weapon == "Dagger" then
			Weapon = "Burtgang"
        end
        windower.add_to_chat('Weapon is now: '..Weapon)
        equip(sets[Weapon])
	elseif command == 'Shield' then
		if Shield == "Aegis" then
            Shield = "Ochain"
		elseif Shield == "Ochain" then
            Shield = "Priwen"
		elseif Shield == "Priwen" then
			Shield = "Aegis"
        end
        windower.add_to_chat('Shield is now: '..Shield)
        equip(sets[Shield])
	end
end

zone_change_delay = 5
windower.raw_register_event('zone change', function (new_id)
	local zones = gearswap.res.zones
	local new_zone = zones[new_id].name
	coroutine.schedule(gearswap.equip_sets:prepare('zone_change', nil, new_zone),(zone_change_delay))
end)

function zone_change(zoneName)
	if zoneName:endswith('Adoulin') then
		equip({ body="Councilor's Garb" })
	end
end
