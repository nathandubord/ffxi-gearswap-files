function get_sets()
    send_command('bind numpad1 gs c TP')
    send_command('bind numpad2 gs c ToggleWeapon')

    sets.TP = {}
    sets.WS = {}
    sets.Weapon = {}

    TPMode = 'Standard'
    Weapon = 'Sword'

    sets.Idle = {
        ammo="Staunch Tathlum +1",
        head={ name="Nyame Helm", augments={'Path: B',}},
        body={ name="Nyame Mail", augments={'Path: B',}},
        hands={ name="Nyame Gauntlets", augments={'Path: B',}},
        legs={ name="Carmine Cuisses +1", augments={'Accuracy+20','Attack+12','"Dual Wield"+6',}},
        feet={ name="Nyame Sollerets", augments={'Path: B',}},
        neck="Warder's Charm +1",
        waist="Plat. Mog. Belt",
        left_ear="Hearty Earring",
        right_ear="Eabani Earring",
        left_ring="Moonlight Ring",
        right_ring="Defending Ring",
        back="Moonlight Cape",
    }

    sets.TP.Standard = {
        ammo={ name="Coiste Bodhar", augments={'Path: A',}},
        head="Flam. Zucchetto +2",
        body="Pelt. Plackart +3",
        hands="Pel. Vambraces +3",
        legs="Ptero. Brais +3",
        feet="Flam. Gambieras +2",
        neck={ name="Dgn. Collar +2", augments={'Path: A',}},
        waist="Sailfi Belt +1",
        left_ear="Crepuscular Earring",
        right_ear="Sherida Earring",
        left_ring="Moonlight Ring",
        right_ring="Chirich Ring +1",
        back={ name="Brigantia's Mantle", augments={'DEX+20','Accuracy+20 Attack+20','Accuracy+10','"Dbl.Atk."+10','Damage taken-5%',}},
    }

    sets.TP.Hybrid = {
        ammo="Staunch Tathlum +1",
        head="Flam. Zucchetto +2",
        body="Hjarrandi Breast.",
        hands="Pel. Vambraces +3",
        legs="Ptero. Brais +3",
        feet="Flam. Gambieras +2",
        neck={ name="Dgn. Collar +2", augments={'Path: A',}},
        waist="Sailfi Belt +1",
        left_ear="Telos Earring",
        right_ear="Sherida Earring",
        left_ring="Moonlight Ring",
        right_ring="Defending Ring",
        back={ name="Brigantia's Mantle", augments={'DEX+20','Accuracy+20 Attack+20','Accuracy+10','"Dbl.Atk."+10','Damage taken-5%',}},
    }

    sets.WS['Stardiver'] = {
        ammo={ name="Coiste Bodhar", augments={'Path: A',}},
        head="Nyame Helm",
        body="Nyame Mail",
        hands="Ptero. Fin. G. +3",
        legs={ name="Nyame Flanchard", augments={'Path: B',}},
        feet="Sulev. Leggings +1",
        neck="Fotia Gorget",
        waist="Fotia Belt",
        left_ear={ name="Moonshade Earring", augments={'Accuracy+4','TP Bonus +250',}},
        right_ear="Sherida Earring",
        left_ring="Sroda Ring",
        right_ring="Moonlight Ring",
        back={ name="Brigantia's Mantle", augments={'DEX+20','Accuracy+20 Attack+20','Accuracy+10','"Dbl.Atk."+10','Damage taken-5%',}},
    }

    sets.WS['Savage Blade'] = {
        ammo="Knobkierrie",
        head="Peltast's Mezail +3",
        body="Pelt. Plackart +3",
        hands="Ptero. Fin. G. +3",
        legs={ name="Nyame Flanchard", augments={'Path: B',}},
        feet={ name="Nyame Sollerets", augments={'Path: B',}},
        neck={ name="Dgn. Collar +2", augments={'Path: A',}},
        waist={ name="Sailfi Belt +1", augments={'Path: A',}},
        left_ear={ name="Moonshade Earring", augments={'Accuracy+4','TP Bonus +250',}},
        right_ear={ name="Peltast's Earring", augments={'System: 1 ID: 1676 Val: 0','Accuracy+10','Mag. Acc.+10',}},
        left_ring="Sroda Ring",
        right_ring="Cornelia's Ring",
        back={ name="Brigantia's Mantle", augments={'STR+20','Accuracy+20 Attack+20','STR+10','Weapon skill damage +10%','Damage taken-5%',}},
    }

    sets.WS['Cataclysm'] = {
        ammo="Knobkierrie",
        head="Pixie Hairpin +1",
        neck="Sibyl Scarf",
        left_ear={ name="Moonshade Earring", augments={'Accuracy+4','TP Bonus +250',}},
        right_ear="Friomisi Earring",
        body="Nyame Mail",
        hands="Ptero. Fin. G. +3",
        left_ring="Archon Ring",
        right_ring="Cornelia's Ring",
        back={ name="Brigantia's Mantle", augments={'STR+20','Accuracy+20 Attack+20','STR+10','Weapon skill damage +10%','Damage taken-5%',}},
        waist="Orpheus's Sash",
        legs={ name="Nyame Flanchard", augments={'Path: B',}},
        feet={ name="Nyame Sollerets", augments={'Path: B',}},
    }

    sets.WS['Sonic Thrust'] = sets.WS['Savage Blade']
    sets.WS['Judgment'] = sets.WS['Savage Blade']
    sets.WS['Retribution'] = sets.WS['Savage Blade']

    sets.Jumps = {
        ammo={ name="Coiste Bodhar", augments={'Path: A',}},
        head="Flam. Zucchetto +2",
        body="Pelt. Plackart +3",
        hands="Vis. Fng. Gaunt. +2",
        legs={ name="Ptero. Brais +3", augments={'Enhances "Strafe" effect',}},
        feet="Ostro Greaves",
        neck="Anu Torque",
        waist={ name="Sailfi Belt +1", augments={'Path: A',}},
        left_ear="Crep. Earring",
        right_ear="Sherida Earring",
        left_ring="Chirich Ring +1",
        right_ring="Chirich Ring +1",
        back={ name="Brigantia's Mantle", augments={'DEX+20','Accuracy+20 Attack+20','Accuracy+10','"Dbl.Atk."+10','Damage taken-5%',}},  
    }
end

function precast(spell)
    if spell.type == "WeaponSkill" then
        if buffactive['terror'] or buffactive['petrified'] or buffactive['sleep'] or buffactive['stun'] or buffactive['amnesia'] or spell.target.distance > 6 then
            cancel_spell()
        else
            equip(sets.WS[spell.english])
        end
    elseif string.find(spell.english, "jump") then
        equip(sets.Jumps)
    elseif spell.english == 'Angon' then
        equip({
            ammo="Angon"
        })
    end
end

function aftercast(spell)
    if player.status == 'Engaged' then
        equip(sets.TP[TPMode])
    else
        equip(sets.Idle)
    end
end

function status_change(new,old)
    if T{'Idle','Resting'}:contains(new) then
        equip(sets.Idle)
    elseif new == 'Engaged' then
        equip(sets.TP[TPMode])
    end
end

function self_command(command)
    if command == 'TP' then
        if TPMode =="Standard" then
            TPMode ="Hybrid"
        elseif TPMode =="Hybrid" then
            TPMode ="Standard"
        end
        windower.add_to_chat('TP mode is now: '..TPMode)
        equip(sets.TP[TPMode])
    elseif command == 'ToggleWeapon' then
        if Weapon == "Sword" then
            Weapon ="Polearm"
            equip({
                main="Trishula",
                sub="Utu Grip",
            })
        elseif Weapon == "Polearm" then
            Weapon ="Club"
            equip({
                main="Mafic Cudgel",
                sub="Regis",
            })
        elseif Weapon == "Club" then
            Weapon = "Staff"
            equip({
                main="Malignance Pole",
                sub="Utu Grip",
            })
        elseif Weapon == "Staff" then
            Weapon = "Sword"
            equip({
                main="Naegling",
                sub="Regis",
            })
        end
        windower.add_to_chat('Weapon is now: '..Weapon)
    end
    if command == 'ws' then
        if Weapon == 'Sword' then
            send_command("input /ws 'Savage Blade' <t>")
        elseif Weapon == "Polearm" then
            send_command("input /ws 'Stardiver' <t>")
        elseif Weapon == "Club" then
            send_command("input /ws 'Judgement' <t>")
        elseif Weapon == "Staff" then
            send_command("input /ws 'Retribution' <t>")
        end

    end
end