include('organizer-lib')

-- Gear Sets
function get_sets()
    sets.TP = {}
    sets.WS = {}

    -- Modes and their default values
    TPMode = 'Standard'

    sets.Idle = {
        ammo="Amar Cluster",
        head="Nyame Helm",
        body="Nyame Mail",
        hands="Nyame Gauntlets",
        legs="Nyame Flanchard",
        feet="Pill. Poulaines +3",
        neck="Loricate Torque +1",
        waist="Platinum moogle belt",
        left_ear="Hearty Earring",
        right_ear="Merman's Earring",
        left_ring="Shadow ring",
        right_ring="Defending Ring",
        back="Shadow Mantle",
    }

    sets.WS['Evisceration'] = {
        ammo="Yetshila +1",
        head={ name="Adhemar Bonnet +1", augments={'STR+12','DEX+12','Attack+20',}},
        body="Gleti's Cuirass",
        hands={ name="Adhemar Wrist. +1", augments={'STR+12','DEX+12','Attack+20',}},
        legs="Gleti's Breeches",
        feet="Gleti's Boots",
        neck="Fotia Gorget",
        waist="Fotia Belt",
        left_ear="Odr Earring",
        right_ear={ name="Moonshade Earring", augments={'Accuracy+4','TP Bonus +250',}},
        left_ring="Gere Ring",
        right_ring="Begrudging Ring",
        back={ name="Toutatis's Cape", augments={'DEX+20','Accuracy+20 Attack+20','DEX+10','Crit.hit rate+10',}},
    }

    sets.WS['Savage Blade'] = {
        ammo="Coiste Bodhar",
        neck="Rep. Plat. Medal",
        head="Nyame Helm",
        body="Nyame Mail",
        hands="Nyame Gauntlets",
        legs="Nyame Flanchard",
        feet="Nyame Sollerets",
        right_ear={ name="Moonshade Earring", augments={'Accuracy+4','TP Bonus +250',}},
        left_ear="Telus earring",
        right_ring="Cornelia's Ring",
        left_ring="Illabrat ring",
        waist="Sailfi Belt +1",
    }

    sets.WS['Aeolian Edge'] = {
        ammo="Ghastly Tathlum +1",
        head="Nyame Helm",
        body="Nyame Mail",
        hands="Nyame Gauntlets",
        legs="Nyame Flanchard",
        feet="Nyame Sollerets",
        neck="Sibyl Scarf",
        waist="Orpheus's Sash",
        left_ear="Friomisi Earring",
        right_ear={ name="Moonshade Earring", augments={'Accuracy+4','TP Bonus +250',}},
        left_ring={ name="Metamor. Ring +1", augments={'Path: A',}},
        right_ring="Cornelia's Ring",
        back={ name="Toutatis's Cape", augments={'INT+20','Mag. Acc+20 /Mag. Dmg.+20','INT+10','Weapon skill damage +10%',}},
    }

    sets.TP.Standard = {
        -- main="Tauret",
        -- sub="Gleti's Knife",
        ammo={ name="Coiste Bodhar", augments={'Path: A',}},
        head="Malignance chapeau",
        body="Pillager's vest +3",
        hands={ name="Adhemar Wrist. +1", augments={'STR+12','DEX+12','Attack+20',}},
        legs="Gleti's Breeches",
        feet="Mummu Gamash. +2",
        neck={ name="Asn. Gorget +1", augments={'Path: A',}},
        waist="Reiki yotai",
        left_ear="Sherida Earring",
        right_ear="Skulker's Earring",
        left_ring="Gere Ring",
        right_ring="Moonlight Ring",
        back={ name="Toutatis's Cape", augments={'DEX+20','Accuracy+20 Attack+20','DEX+2','"Store TP"+10',}},
    }

    -- TH 3 from Thief traits
    sets.TP.TH = set_combine(sets.TP.Standard, {
        ammo="Per. Lucky Egg", -- TH 1
        hands={ name="Plun. Armlets +3", augments={'Enhances "Perfect Dodge" effect',}}, -- TH 4
        feet="Skulk. Poulaines +3", -- TH 5
    })

    sets.TP.Domain = {
        main="Voluspa Knife",
        sub="Gleti's Knife",
        ammo="Yetshila +1",
        head={ name="Blistering Sallet +1", augments={'Path: A',}},
        body="Gleti's Cuirass",
        hands="Gleti's Gauntlets",
        legs="Mummu Kecks +2",
        feet="Mummu Gamash. +2",
        neck={ name="Asn. Gorget +1", augments={'Path: A',}},
        waist="Reiki Yotai",
        left_ear="Odr Earring",
        right_ear="Eabani Earring",
        left_ring="Warp Ring",
        right_ring="Gere Ring",
        back={ name="Toutatis's Cape", augments={'DEX+20','Accuracy+20 Attack+20','DEX+10','Crit.hit rate+10',}},
    }

    sets.Flee = { feet="Pill. Poulaines +3", }
end

function precast(spell)
	if spell.type == "WeaponSkill" then
        equip(sets.WS[spell.english])
    end
end

function aftercast(spell)
    if player.status == 'Engaged' then
        equip(sets.TP[TPMode])
    else
        equip(sets.Idle)
    end
end

function status_change(new,old)
    if T{'Idle','Resting'}:contains(new) then
        equip(sets.Idle)
    elseif new == 'Engaged' then
        equip(sets.TP[TPMode])
    end
end

function self_command(command)
    if command == 'TP' then
        if TPMode =="Standard" then
            TPMode = "TH"
        elseif TPMode == "TH" then
            TPMode = "Domain"
        elseif TPMode == "Domain" then
            TPMode = "Standard"
        end
        windower.add_to_chat('TP mode is now: '..TPMode)
        equip(sets.TP[TPMode])
    end
end
